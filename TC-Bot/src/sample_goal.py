import random
import json
from deep_dialog.dialog_system import text_to_dict
import cPickle as pickle

def sample_goals():
    slot_set_path = './deep_dialog/data/slot_set_small.txt'
    movie_kb_path = './deep_dialog/data/movie_kb.1k.p'
    goal_path = './deep_dialog/data/goal.new.v1'
    slot_set = text_to_dict(slot_set_path)
    movie_kb = pickle.load(open(movie_kb_path, 'rb'))
    feeds = []

    for line in movie_kb.values():
        if line.has_key("moviename"):
            new_goal = generate_goals(line, slot_set)
            if new_goal not in feeds:
                feeds.append(new_goal)
                print(len(feeds))
    with open(goal_path, 'w') as feedsjson:
        json.dump(feeds, feedsjson, indent=4)


def generate_goals(movie_line, slot):
    request_slots = {"ticket": "UNK"}
    inform_slots = {"moviename": "PLACEHOLDER"}
    print(movie_line)
    if movie_line.has_key("state"):
        del movie_line["state"]
    if movie_line.has_key("zip"):
        del movie_line["zip"]
    inform_slots["moviename"] = movie_line["moviename"].strip().split('#')[0]
    _ = movie_line.pop("moviename")
    if movie_line.has_key("city"):
        inform_slots["city"]=movie_line["city"].strip().split('#')[0]
        _ = movie_line.pop("city")
    if movie_line.has_key("numberofpeople"):
        inform_slots["numberofpeople"]=movie_line["numberofpeople"].strip().split('#')[0]
        _ = movie_line.pop("numberofpeople")
    else:
        inform_slots["numberofpeople"] = "1"
    movie_line_list = movie_line.keys()
    random.shuffle(movie_line_list)
    for index,item in enumerate(movie_line_list):
        if index%2==0 and item in slot:
            request_slots[item] = "UNK"
        elif index%2==1 and item in slot:
            inform_slots[item] = movie_line[item].strip().split('#')[0]
    final_goal = {"request_slots":request_slots,  "diaact": "request", "inform_slots": inform_slots}
    return final_goal


if __name__ == "__main__":
    sample_goals()