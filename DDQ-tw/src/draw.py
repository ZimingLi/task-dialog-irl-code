import numpy as np
import matplotlib.pyplot as plt
min_x =0
max_x = 500

def extract_tc_score(file_name):
    episode=[]
    score = []
    with open(file_name, 'r') as f_s:
        line = f_s.readline()
        while line:
            line = line.strip().split()
            episode.append(int(line[1]))
            score.append(float(line[5].split(',')[0]))
            line = f_s.readline()
    return np.array(episode), np.array(score)

def extract_irl_score(file_name):
    episode=[]
    score = []
    count=0
    with open(file_name, 'r') as f_s:
        line = f_s.readline()
        while line:
            count+=1
            line = line.strip().split()
            if int(line[-1])%30==0:
                episode.append(int(line[-1]))
                score.append(float(line[2].split(',')[0]))
            line = f_s.readline()
    return np.array(episode), np.array(score)

def draw_score(reward_id, fold_id):
    # episode_tc, score_tc = extract_tc_score('./result/tc_fold_{}.log'.format(fold_id))
    # episode_seqgan, score_seqgan = extract_tc_score('./reuse_result/tc_fold_{}.log'.format(fold_id+1))
    # episode_irl, score_irl = extract_irl_score('./reuse_result/irl_fold_{}.log'.format(fold_id+1))
    # episode_seqgan, score_seqgan = extract_irl_score('./result_for_irl_seqgan/norm_result/irl_r5id3re_reward_{}.log'.format(fold_id+1))

    episode_seqgan, score_seqgan = extract_irl_score('./result_for_irl_seqgan/norm_result/seqgan_lr0005_hid100_lambda_0.001_max_30_device_cpu_time_20190421_t_fold_{}.pt'.format(fold_id))
    # episode_seqgan, score_seqgan = extract_irl_score('./result_for_irl_seqgan/logs_h1/seqgan_noise_0.2_every_10_lr001_hid100_lambda_0.001_max_30_device_cpu_time_20190426_t_fold_{}.pt'.format(fold_id))

    episode_irl, score_irl = extract_irl_score('./result_for_irl_seqgan/logs_h1/irl_{}_reward_{}.log'.format(reward_id, fold_id+1))
    # episode_irl, score_irl = extract_irl_score('./result_for_irl_seqgan/logs_h1/irl_{}_reward_{}.log'.format(reward_id, fold_id+1))
    #
    # episode_seq2seq, score_seq2seq = extract_irl_score(
    #         './result_for_irl_seqgan/logs_h1/seq2seq_noise_0.2_every_10_lr001_hid100_lambda_0.001_max_30_device_cpu_time_20190426_t_fold_{}.pt'.format(fold_id))
    episode_seq2seq, score_seq2seq = extract_irl_score(
        './result_for_irl_seqgan/norm_result/seq2seq_lr001_hid100_lambda_0.001_max_30_device_cpu_time_20190416_t_fold_{}.pt'.format(
            fold_id))
    # episode_seq2seq, score_seq2seq = extract_irl_score(
    #         './result_for_irl_seqgan/logs_h1/seq2seq_noise_0.0_every_10_lr001_hid100_lambda_0.001_max_30_device_cpu_time_20190519_v2_t_fold_{}.pt'.format(fold_id))

    score_seq2seq = score_seq2seq[5:]
    score_irl[0] = score_seq2seq[0]
    score_seqgan[0] = score_seq2seq[0]

    '''
    pre_num=5
    episode_irl = np.concatenate((episode_seq2seq[:pre_num] , episode_irl))
    score_irl = np.concatenate((score_seq2seq[:pre_num] , score_irl))
    episode_seqgan=np.concatenate((episode_seq2seq[:pre_num], episode_seqgan))
    score_seqgan = np.concatenate((score_seq2seq[:pre_num], score_seqgan))
    '''
    # episode_seqgan, score_seqgan = extract_irl_score(
    #     './result_for_irl_seqgan/result_pre300/seqgan_pre300_hid100_lambda_0.001_max_30_device_cpu_time_20190225-1815_t_fold_{}.pt'.format(
    #         fold_id))
    # episode_irl, score_irl = extract_irl_score(
    #     './result_for_irl_seqgan/result_pre300/rs_h100_pre300_irl_1000e_{}.pt'.format(
    #         fold_id+6))
    # episode_seq2seq, score_seq2seq = extract_irl_score(
    #     './result_for_irl_seqgan/seq2seq_result/seq2seq_hid100_lambda_0.001_max_30_device_cpu_time_20190226-1834_t_fold_{}.pt'.format(
    #         fold_id))


    v_max = 1.0
    v_min = 0
    mk = ('4', '+', '.', '2', '|', 4, '1')
    colors = (
        '#d73027',
        '#fc8d59',
        '#fee090',
        '#e0f3f8',
        '#91bfdb',
        '#4575b4'

    )
    '''
    lst = (':', '-', '-.', '--')

    fig = plt.figure(figsize=(20, 10))

    plt.plot(episode_seqgan, score_seqgan, label="GAN-BOT", color=colors[0], linewidth=1, marker=mk[0])
    plt.plot(episode_irl, score_irl, label="IRL-BOT", color=colors[1], linewidth=1, marker=mk[1])
    plt.plot(episode_seq2seq, score_seq2seq, label="S2S-BOT", color=colors[2], linewidth=1, marker=mk[2])


    plt.axis([min_x, max_x, v_min, v_max])
    plt.yticks(np.arange(v_min, v_max, 0.05))
    plt.xticks(np.arange(min_x, max_x, 100))
    plt.xlabel("Epoch number", fontsize=13)

    plt.ylabel("Success rate", fontsize=13)
    leg = plt.legend(loc=0, fancybox=True, fontsize=12)
    leg.get_frame().set_alpha(0.5)
    # plt.show()
    # fig.savefig('./figures/new_fold_{}.png'.format(fold_id), format='png', dpi=600)
    '''
    min_len = 36
    return episode_seqgan[:min_len], score_seqgan[:min_len], episode_irl[:min_len], score_irl[:min_len], episode_seq2seq[:min_len], score_seq2seq[:min_len]

def draw_final_figure(reward_id, episode_tc_all, score_tc_all, std_tc_all, episode_irl_all, score_irl_all, std_irl_all, episode_s2s_all, score_s2s_all, std_s2s_all):
    v_max = 1.0
    v_min = 0
    # f3.errorbar(X, state_value_maxent_array_average[0],yerr=std_x*state_value_maxent_array_std[0],label="Opt-NoFeedback", color=colors[0], linewidth=1, marker=mk[1], )
    mk = ('4', '+', '.', '2', '|', 4, '1')
    colors = (
        '#d7191c',
        '#fdae61',
        '#ffffbf',
        '#abd9e9',
        '#91bfdb',
        '#2c7bb6'

    )
    lst = (':', '-', '-.', '--')
    '''
    fig3, f3 = plt.subplots(figsize=(20, 10))
    f3.plot(episode_tc_all, score_tc_all, label="GAN-BOT", color=colors[0], linewidth=1, marker=mk[0])
    f3.plot(episode_irl_all, score_irl_all, label="IRL-BOT", color=colors[1], linewidth=1, marker=mk[1])

    f3.axis([-2100, 3000, v_min, v_max])
    f3.yticks(np.arange(v_min, v_max, 0.05))
    f3.xticks(np.arange(-2100, 3000, 400))
    f3.xlabel("Epoch number", fontsize=13)

    f3.ylabel("Success rate", fontsize=13)
    leg = f3.legend(loc=0, fancybox=True, fontsize=12)
    leg.get_frame().set_alpha(0.5)
    fig3.savefig('./figures/final.jpg', format='jpeg', dpi=600)

    '''
    fig = plt.figure(figsize=(12, 9))
    plt.plot(episode_tc_all, score_tc_all, label="Adv-Bot", color=colors[3], linewidth=1, marker=mk[0])
    plt.plot(episode_irl_all, score_irl_all, label="IRL-Bot", color=colors[0], linewidth=1, marker=mk[1])
    plt.plot(episode_irl_all, score_s2s_all, label="Sup-Bot", color=colors[4], linewidth=1, marker=mk[2])

    # plt.errorbar(episode_tc_all, score_tc_all,yerr=std_tc_all/np.sqrt(5),label="GAN-BOT", color=colors[0], linewidth=1, marker=mk[0], )
    # plt.errorbar(episode_irl_all, score_irl_all,yerr=std_irl_all/np.sqrt(5),label="IRL-BOT", color=colors[1], linewidth=1, marker=mk[1], )
    score_tc_se = std_tc_all/np.sqrt(5)
    score_irl_se = std_irl_all/np.sqrt(5)
    score_s2s_se = std_s2s_all/np.sqrt(5)
    # '''
    plt.fill_between(episode_tc_all, score_tc_all - score_tc_se, score_tc_all + score_tc_se,
                    alpha=0.2, edgecolor=colors[3], facecolor=colors[3])
    plt.fill_between(episode_irl_all, score_irl_all - score_irl_se, score_irl_all + score_irl_se,
                     alpha=0.2, edgecolor=colors[0], facecolor=colors[0])
    plt.fill_between(episode_irl_all, score_s2s_all - score_s2s_se, score_s2s_all + score_s2s_se,
                     alpha=0.2, edgecolor=colors[4], facecolor=colors[4])
    # '''
    plt.axis([min_x, max_x, v_min, v_max])
    plt.yticks(np.arange(v_min, v_max, 0.05))
    plt.xticks(np.arange(min_x, max_x, 30))
    plt.xlabel("Epoch number", fontsize=21)

    plt.ylabel("Success rate", fontsize=21)
    leg = plt.legend(loc=4, fancybox=True, fontsize=21)
    leg.get_frame().set_alpha(0.5)
    # plt.show()
    fig.savefig('./figures/{}.jpg'.format(reward_id), format='jpeg', dpi=600)
    plt.show()

def draw_all_folds(reward_id):
    episode_tc_all, score_tc_all, episode_irl_all, score_irl_all,  episode_s2s_all, score_s2s_all= [], [], [], [],[],[]
    for i in range(0,5):
        episode_tc, score_tc, episode_irl, score_irl, episode_s2s, score_s2s = draw_score(reward_id, i)
        episode_tc_all.append(episode_tc[:170])
        score_tc_all.append(score_tc[:170])
        episode_irl_all.append(episode_irl[:170])
        score_irl_all.append(score_irl[:170])
        episode_s2s_all.append(episode_s2s[:170])
        score_s2s_all.append(score_s2s[:170])
    episode_tc_all = np.array(episode_tc_all).mean(axis=0)
    score_tc_avg = np.array(score_tc_all).mean(axis=0)
    print("seqgan: {}".format(np.max(score_tc_avg)))
    std_tc_all = np.array(score_tc_all).std(axis=0)
    episode_irl_all = np.array(episode_irl_all).mean(axis=0)
    score_irl_avg = np.array(score_irl_all).mean(axis=0)
    print("irl: {}".format(np.max(score_irl_avg)))
    std_irl_all = np.array(score_irl_all).std(axis=0)
    episode_s2s_all = np.array(episode_s2s_all).mean(axis=0)
    score_s2s_avg = np.array(score_s2s_all).mean(axis=0)
    print("seq2seq: {}".format(np.max(score_s2s_avg)))
    std_s2s_all = np.array(score_s2s_all).std(axis=0)
    draw_final_figure(reward_id, episode_tc_all, score_tc_avg,std_tc_all, episode_irl_all, score_irl_avg, std_irl_all, episode_s2s_all, score_s2s_avg, std_s2s_all)

def extract_seqgan_from_log(reward_id, job_id):
    # extract result from log files
    def extract_score(file_name, target_name):
        with open(file_name, 'r') as file_h:
            with open(target_name, 'w') as file_t:
                line = file_h.readline()
                count = 0
                while line:
                    line_str = line.strip().split('========== ')
                    if len(line_str) > 1:
                        if line_str[1].split()[0] == 'Success':
                            count += 1
                            file_t.write(line_str[1] + '\n')
                    line = file_h.readline()
                print(count)
    for fold_id in range(0,5):
        # file_name = "result_for_irl_seqgan/norm_result/irl_r9t8_h100_pre300_irl_297871_{}.log".format(fold_id+1)
        # target_name = "result_for_irl_seqgan/norm_result/irl_r9t8_reward_{}.log".format(fold_id+1)
        file_name = "result_for_irl_seqgan/logs_h1/irl_{}_{}_{}.log".format(reward_id, job_id, fold_id + 1)
        target_name = "result_for_irl_seqgan/logs_h1/irl_{}_reward_{}.log".format(reward_id, fold_id + 1)

        # file_name = "result_for_irl_seqgan/reuse_result/irl_8beam_h100_pre300_irl_286752_{}.log".format(
        #     fold_id + 1)
        # target_name = "result_for_irl_seqgan/reuse_result/irl_8beam_{}.log".format(fold_id + 1)
        extract_score(file_name, target_name)

def error_type():

    def retrieve_error_type(file_name, fold):
        epoch_count = 0
        if fold in [1,2,3]:
            flag_list = [0] * 127
        else:
            flag_list = [0] * 126
        with open(file_name,'r') as file_handle:
            line = file_handle.readline()
            while line:
                line_list = line.strip().split()
                if line_list[0]=='dialogue':
                    idx = int(line_list[2].split(',')[0])
                    if idx == 0:
                        epoch_count += 1
                    if line_list[3]=='Success':
                        flag_list[idx] += 1
                line = file_handle.readline()
        print (epoch_count)
        return flag_list

    def extract_result(method):
        dis_list = []
        for fold_id in range(1,6):
            if method=='irl':
                file_name = 'result_for_irl_seqgan/log_acl/rs_new_h100_pre300_irl_275073_{}.log'.format(fold_id)
            elif method=='seqgan':
                file_name = 'result_for_irl_seqgan/log_acl/seqgan_lr002_h100_275145_{}.log'.format(fold_id)
            elif method == 'seq2seq':
                file_name = 'result_for_irl_seqgan/log_acl/seq2seq_lr002_h100_275150_{}.log'.format(fold_id)
            result_list = retrieve_error_type(file_name, fold_id)
            dis_list += result_list
        return dis_list

    dis_irl = extract_result('irl')
    dis_irl = np.array(dis_irl)*1.0/13
    dis_seqgan = extract_result('seqgan')
    dis_seqgan = np.array(dis_seqgan)*1.0/13
    dis_seq2seq = extract_result('seq2seq')
    dis_seq2seq = np.array(dis_seq2seq) * 1.0 /13
    x_list = range(0, len(dis_irl))
    v_max = 1.0
    v_min = 0
    mk = ('4', '+', '.', '2', '|', 4, '1')
    colors = (
        '#d73027',
        '#fc8d59',
        '#fee090',
        '#e0f3f8',
        '#91bfdb',
        '#4575b4'

    )
    lst = (':', '-', '-.', '--')
    fig = plt.figure(figsize=(20, 10))
    plt.plot(x_list, dis_irl, label="IRL-BOT", color=colors[0], linewidth=1, marker=mk[0])
    plt.plot(x_list, dis_seqgan, label="ADV-BOT", color=colors[5], linewidth=1, marker=mk[1])
    plt.plot(x_list, dis_seq2seq, label="S2S-BOT", color=colors[4], linewidth=1, marker=mk[2])


    plt.axis([0, 100, 0.4, 1.1])
    # plt.yticks(np.arange(v_min, v_max, 0.05))
    plt.xticks(np.arange(0, 100, 10))
    plt.xlabel("Task ID", fontsize=13)

    plt.ylabel("Success rate", fontsize=13)
    leg = pltcd .legend(loc=4, fancybox=True, fontsize=12)
    leg.get_frame().set_alpha(0.1)
    plt.show()
    # fig.savefig('./figures/error_type.jpg', format='jpeg', dpi=600)



if __name__ == "__main__":
    def pipeline(reward_id, job_id):
        extract_seqgan_from_log(reward_id, job_id)
        draw_all_folds(reward_id)
    # extract_seqgan_from_log()
    # draw_all_folds()
    # error_type()
    for i in range(11,12):
        reward_id = "r11t9"
        job_id = str(654 + 5 * (1-1))
        pipeline(reward_id, job_id)