"""
Created on May 22, 2016

This should be a simple minimalist run file. It's only responsibility should be to parse the arguments (which agent, user simulator to use) and launch a dialog simulation.

@author: xiul, t-zalipt, baolin
"""

import argparse, json, copy, os
# Todo
import cPickle as pickle
# import pickle as pickle

from deep_dialog.dialog_system import text_to_dict

from deep_dialog import dialog_config
from deep_dialog.dialog_config import *

from deep_dialog.nlu import nlu
from deep_dialog.nlg import nlg
from deep_dialog.adv_learning.DialogAdvTraining import DialogAdvTrain

import numpy
import random

seed = 5
numpy.random.seed(seed)
random.seed(seed)

import torch

""" 
Launch a dialog simulation per the command line arguments
This function instantiates a user_simulator, an agent, and a dialog system.
Next, it triggers the simulator to run for the specified number of episodes.
"""

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--dict_path', dest='dict_path', type=str, default='./deep_dialog/data/dicts.v3.p.new',
                        help='path to the .json dictionary file')
    parser.add_argument('--movie_kb_path', dest='movie_kb_path', type=str, default='./deep_dialog/data/movie_kb.1k.p.new',
                        help='path to the movie kb .json file')
    parser.add_argument('--act_set', dest='act_set', type=str, default='./deep_dialog/data/dia_acts.txt',
                        help='path to dia act set; none for loading from labeled file')
    parser.add_argument('--slot_set', dest='slot_set', type=str, default='./deep_dialog/data/slot_set.txt',
                        help='path to slot set; none for loading from labeled file')
    parser.add_argument('--goal_file_path', dest='goal_file_path', type=str,
                        default='./deep_dialog/data/user_goals_first_turn_template.part.movie.v1.p.new',
                        help='a list of user goals')
    parser.add_argument('--expert_file_path', dest='expert_file_path', type=str,
                        default='./deep_dialog/data/success_data_train_fold_0.json',
                        help='expert behaviors')
    parser.add_argument('--diaact_nl_pairs', dest='diaact_nl_pairs', type=str,
                        default='./deep_dialog/data/dia_act_nl_pairs.v6.json',
                        help='path to the pre-defined dia_act&NL pairs')

    parser.add_argument('--max_turn', dest='max_turn', default=40, type=int,
                        help='maximum length of each dialog (default=20, 0=no maximum length)')
    parser.add_argument('--episodes', dest='episodes', default=1, type=int,
                        help='Total number of episodes to run (default=1)')
    parser.add_argument('--slot_err_prob', dest='slot_err_prob', default=0.00, type=float,
                        help='the slot err probability')
    parser.add_argument('--slot_err_mode', dest='slot_err_mode', default=0, type=int,
                        help='slot_err_mode: 0 for slot_val only; 1 for three errs')
    parser.add_argument('--intent_err_prob', dest='intent_err_prob', default=0.05, type=float,
                        help='the intent err probability')

    parser.add_argument('--agt', dest='agt', default=9, type=int,
                        help='Select an agent: 0 for a command line input, 1-6 for rule based agents')
    parser.add_argument('--usr', dest='usr', default=1, type=int,
                        help='Select a user simulator. 0 is a Frozen user simulator.')

    parser.add_argument('--epsilon', dest='epsilon', type=float, default=0,
                        help='Epsilon to determine stochasticity of epsilon-greedy agent policies')

    # load NLG & NLU model
    parser.add_argument('--nlg_model_path', dest='nlg_model_path', type=str,
                        default='./deep_dialog/models/nlg/lstm_tanh_relu_1468202263.38_2_0.610.p.new',
                        help='path to model file')
    parser.add_argument('--nlu_model_path', dest='nlu_model_path', type=str,
                        default='./deep_dialog/models/nlu/lstm_1468447442.91_39_80_0.921.p',
                        help='path to the NLU model file')

    parser.add_argument('--act_level', dest='act_level', type=int, default=0,
                        help='0 for dia_act level; 1 for NL level')
    parser.add_argument('--run_mode', dest='run_mode', type=int, default=2,
                        help='run_mode: 0 for default NL; 1 for dia_act; 2 for both')
    parser.add_argument('--auto_suggest', dest='auto_suggest', type=int, default=0,
                        help='0 for no auto_suggest; 1 for auto_suggest')
    parser.add_argument('--cmd_input_mode', dest='cmd_input_mode', type=int, default=0,
                        help='run_mode: 0 for NL; 1 for dia_act')

    # RL agent parameters
    parser.add_argument('--experience_replay_pool_size', dest='experience_replay_pool_size', type=int, default=5000,
                        help='the size for experience replay')
    parser.add_argument('--dqn_hidden_size', dest='dqn_hidden_size', type=int, default=80,
                        help='the hidden size for DQN')
    parser.add_argument('--batch_size', dest='batch_size', type=int, default=2, help='batch size')
    parser.add_argument('--beam_size', dest='beam_size', type=int, default=2, help='beam size')
    parser.add_argument('--rule_data_size', dest='rule_data_size', type=int, default=2, help='rule data size')

    parser.add_argument('--gamma', dest='gamma', type=float, default=0.9, help='gamma for DQN')
    parser.add_argument('--lambda_factor', dest='lambda_factor', type=float, default=0.0, help='lambda_factor for entropy')
    parser.add_argument('--predict_mode', dest='predict_mode', type=bool, default=False, help='predict model for DQN')
    parser.add_argument('--simulation_epoch_size', dest='simulation_epoch_size', type=int, default=50,
                        help='the size of validation set')
    parser.add_argument('--warm_start', dest='warm_start', type=int, default=0,
                        help='0: no warm start; 1: warm start for training')
    parser.add_argument('--warm_start_epochs', dest='warm_start_epochs', type=int, default=100,
                        help='the number of epochs for warm start')
    parser.add_argument('--planning_steps', dest='planning_steps', type=int, default=4,
                        help='the number of planning steps')

    parser.add_argument('--trained_model_path', dest='trained_model_path', type=str, default=None,
                        help='the path for trained model')
    parser.add_argument('--model_path', dest='model_path', type=str, default='./deep_dialog/checkpoints/',
                        help='the path for trained model')

    parser.add_argument('-o', '--write_model_dir', dest='write_model_dir', type=str,
                        default='./deep_dialog/checkpoints/', help='write model to disk')
    parser.add_argument('--save_check_point', dest='save_check_point', type=int, default=10,
                        help='number of epochs for saving model')

    parser.add_argument('--pre_start_time', dest='pre_start_time', default='20190225-1659', type=str,
                        help='the start time of the loaded model')

    # We changed to have a queue to hold experences. So this threshold will not be used to flush the buffer.
    parser.add_argument('--success_rate_threshold', dest='success_rate_threshold', type=float, default=0.6,
                        help='the threshold for success rate')

    parser.add_argument('--split_fold', dest='split_fold', default=5, type=int,
                        help='the number of folders to split the user goal')
    parser.add_argument('--learning_phase', dest='learning_phase', default='all', type=str,
                        help='train/test/all; default is all')

    parser.add_argument('--grounded', dest='grounded', type=int, default=0,
                        help='planning k steps with environment rather than world model')
    parser.add_argument('--boosted', dest='boosted', type=int, default=1, help='Boost the world model')
    parser.add_argument('--train_world_model', dest='train_world_model', type=int, default=1,
                        help='Whether train world model on the fly or not')
    parser.add_argument('--torch_seed', dest='torch_seed', type=int, default=100, help='random seed for troch')
    parser.add_argument('--teacher_forcing', dest='teacher_forcing_flag', action='store_true', help='teacher_forcing_flag')
    parser.add_argument('--no_teacher_forcing', dest='teacher_forcing_flag', action='store_false', help='teacher_forcing_flag')
    parser.add_argument('--fold_id', dest='fold_id', default=5, type=int, help='the id of test fold')


    args = parser.parse_args()
    params = vars(args)

    print('Dialog Parameters: ')
    print(json.dumps(params, indent=2))

max_turn = params['max_turn']
num_episodes = params['episodes']

agt = params['agt']
usr = params['usr']

dict_path = params['dict_path']
goal_file_path = params['goal_file_path']
fold_id = params['fold_id']
# load the user goals from .p file
# all_goal_set = pickle.load(open(goal_file_path, 'rb'))
all_goal_set = json.load(open(goal_file_path, 'r'))

# split goal set
split_fold = params.get('split_fold', 5)
goal_set = {'train': [], 'valid': [], 'test': [], 'all': []}
for u_goal_id, u_goal in enumerate(all_goal_set):
    if u_goal_id % split_fold == fold_id: goal_set['test'].append(u_goal)
    else: goal_set['train'].append(u_goal)
    goal_set['all'].append(u_goal)
# end split goal set

movie_kb_path = params['movie_kb_path']
movie_kb = pickle.load(open(movie_kb_path, 'rb'))

act_set = text_to_dict(params['act_set'])
slot_set = text_to_dict(params['slot_set'])

################################################################################
# a movie dictionary for user simulator - slot:possible values
################################################################################
movie_dictionary = pickle.load(open(dict_path, 'rb'))

dialog_config.run_mode = params['run_mode']
dialog_config.auto_suggest = params['auto_suggest']

################################################################################
#   Parameters for Agents
################################################################################
agent_params = {}
agent_params['max_turn'] = max_turn
agent_params['model_path'] = params['model_path']
agent_params['lambda_factor'] = params['lambda_factor']
agent_params['epsilon'] = params['epsilon']
agent_params['agent_run_mode'] = params['run_mode']
agent_params['agent_act_level'] = params['act_level']
agent_params['beam_size'] = params['beam_size']
agent_params['rule_data_size'] = params['rule_data_size']
agent_params['experience_replay_pool_size'] = params['experience_replay_pool_size']
agent_params['dqn_hidden_size'] = params['dqn_hidden_size']
agent_params['batch_size'] = params['batch_size']
agent_params['gamma'] = params['gamma']
agent_params['predict_mode'] = params['predict_mode']
agent_params['trained_model_path'] = params['trained_model_path']
agent_params['warm_start'] = params['warm_start']
agent_params['cmd_input_mode'] = params['cmd_input_mode']
agent_params['teacher_forcing_flag'] = params['teacher_forcing_flag']
agent_params['expert_file'] = params['expert_file_path']
agent_params['fold_id'] = params['fold_id']
agent_params['pre_start_time']=params['pre_start_time']
# Manually set torch seed to ensure fail comparison.
torch.manual_seed(params['torch_seed'])


################################################################################
#   Parameters for User Simulators
################################################################################
usersim_params = {}
usersim_params['max_turn'] = max_turn
usersim_params['slot_err_probability'] = params['slot_err_prob']
usersim_params['slot_err_mode'] = params['slot_err_mode']
usersim_params['intent_err_probability'] = params['intent_err_prob']
usersim_params['simulator_run_mode'] = params['run_mode']
usersim_params['simulator_act_level'] = params['act_level']
usersim_params['learning_phase'] = params['learning_phase']
usersim_params['hidden_size'] = params['dqn_hidden_size']

################################################################################
# load trained NLG model
################################################################################
nlg_model_path = params['nlg_model_path']
diaact_nl_pairs = params['diaact_nl_pairs']
nlg_model = nlg()
nlg_model.load_nlg_model(nlg_model_path)
nlg_model.load_predefine_act_nl_pairs(diaact_nl_pairs)
# will be loaded inside the training class
################################################################################
# load trained NLU model
################################################################################
nlu_model_path = params['nlu_model_path']
nlu_model = nlu()
nlu_model.load_nlu_model(nlu_model_path)
# will be loaded inside the training class
################################################################################
# Dialog Trainer
################################################################################
# print(dialog_config.run_mode)

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# Assume that we are on a CUDA machine, then this should print a CUDA device:
print(device)

adv_trainer = DialogAdvTrain(state_input_dim=20, action_embedding_dim=30, rnn_hidden_dim=100, use_cuda=None,
                             agent_params=agent_params, user_params=usersim_params,
                             act_set=act_set, slot_set=slot_set, start_set=goal_set, movie_dictionary=movie_kb,
                             max_turn=max_turn, dropout=0.0, movie_kb=movie_kb,
                             nlg_model=nlg_model, nlu_model=nlu_model, device=device)

################################################################################
#   Run num_episodes Conversation Simulations
################################################################################
# adv_trainer.adv_training()
adv_trainer.gan_training()