import torch
import copy
import numpy as np
import json

class DiscData():
    def __init__(self, max_turn, num_actions, feasible_actions, device):
        self.max_turn = max_turn
        self.num_actions = num_actions
        self.feasible_actions = feasible_actions
        self.device = device

    def padding(self, dialogues_to_be_padded):
        '''
        :param dialogues_to_be_padded: list
        :return: batch_size * len * dic
        '''
        dialogues_padded = []
        mask = []
        state_len = len(dialogues_to_be_padded[0][0]["state_rnn_input"])
        action_len = len(self.feasible_actions)
        for each_line in dialogues_to_be_padded:
            length = len(each_line)
            padded_length = self.max_turn / 2 - length
            new_line = []
            for turn in each_line:
                new_turn = {}
                new_turn['state_rnn_input'] = turn["state_rnn_input"]
                new_turn['action'] = self.action_index(turn['action'])
                new_turn['action_id'] = turn['action_id']
                new_turn['state_action_input'] = new_turn['state_rnn_input'] + new_turn['action']
                new_line.append(new_turn)

            for _ in range(padded_length):
                new_turn = {}
                new_turn['state_rnn_input'] = [0] * state_len
                new_turn['action'] = [0] * action_len
                new_turn['action_id'] = 0
                new_turn['state_action_input'] = new_turn['state_rnn_input'] + new_turn['action']
                new_line.append(new_turn)
            mask_line = [1] * length + [0] * padded_length
            dialogues_padded.append(new_line)
            mask.append(mask_line)
        return dialogues_padded, mask

    def action_index(self, action):
        """ Return the one_hotof action """
        agent_slot_response = action['act_slot_response']
        agent_rep = np.zeros((1, self.num_actions))
        for (i, action_each) in enumerate(self.feasible_actions):
            if agent_slot_response == action_each:
                agent_rep[0][i] = 1.0
        return agent_rep.ravel().tolist()

    def reward(self, disc_model, dialogues, gamma):
        '''
        :param disc_model:
        :param dialogues: batch * max_turn * dim
        :param gamma:
        :return:
        '''
        # 0->machine, 1->human
        batch_size, len_seq, _ = dialogues.size()
        _, reward_soft = disc_model.get_reward(dialogues)
        # reward_soft: batch_size * num_class
        reward_for_agent = []
        discount = 1.0
        for ix in range(len_seq):
            reward_for_agent.append(discount * reward_soft[:,1])
            discount *= gamma
        return torch.stack(reward_for_agent).to(self.device)


    def prepare_input_for_disc(self, dialogues_padded):
        """
        :param dialogue: (batch_size * beam_size) * max_turn
        :return: batch * max_turn * dim
        """
        dialogue_flatten_state = []
        # print(len(dialogue_beam_sampled), len(dialogue_beam_sampled[0]), len(dialogue_beam_sampled[1][0]))
        for dialog_l in dialogues_padded:
            full_dialog_state = []
            for each_sa in dialog_l:
                # print(each_sa)
                state_action_pair = each_sa['state_action_input']
                full_dialog_state.append(torch.FloatTensor(state_action_pair))
            full_dialog_state = torch.stack(full_dialog_state)
            dialogue_flatten_state.append(full_dialog_state)
        dialogue_flatten_state = torch.stack(dialogue_flatten_state)
        # max_turn * batch_size * dim
        # dialogue_flatten_state = dialogue_flatten_state.permute(1, 0, 2)
        return dialogue_flatten_state.to(self.device)


    def prepare_input_for_agent(self, dialogues):
        """
        :param dialogue_beam_sampled: (batch_size * beam_size) * max_turn
        :return: max_turn * (batch_size * beam_size)
        """
        dialogue_flatten_state = []
        dialogue_flatten_action = []
        dialogue_flatten_action_id = []
        # print(len(dialogue_beam_sampled), len(dialogue_beam_sampled[0]), len(dialogue_beam_sampled[1][0]))
        for dialog_l in dialogues:
            full_dialog_state = []
            full_dialog_action = []
            full_dialog_action_id = []
            for each_sa in dialog_l:
                # print(each_sa)
                state = each_sa['state_rnn_input']
                # print(each_sa['action'])
                action = each_sa['action']
                # action = action.ravel().tolist()
                action_id = each_sa['action_id']
                state = torch.FloatTensor(state)
                # this is the ine-hot representation of predicted action
                action = torch.FloatTensor(action)
                # this is the index of predicted action
                # action_id = torch.LongTensor(action_id)
                full_dialog_state.append(copy.deepcopy(state))
                full_dialog_action.append(copy.deepcopy(action))
                # this is the index of predicted action
                full_dialog_action_id.append(copy.deepcopy(action_id))
            full_dialog_state = torch.stack(full_dialog_state)
            full_dialog_action = torch.stack(full_dialog_action)
            full_dialog_action_id = torch.LongTensor(full_dialog_action_id)
            dialogue_flatten_state.append(full_dialog_state)
            dialogue_flatten_action.append(full_dialog_action)
            dialogue_flatten_action_id.append(full_dialog_action_id)

        dialogue_flatten_state = torch.stack(dialogue_flatten_state)
        dialogue_flatten_action = torch.stack(dialogue_flatten_action)
        dialogue_flatten_action_id = torch.stack(dialogue_flatten_action_id)
        # max_turn * (batch_size * beam_size)
        dialogue_flatten_state = dialogue_flatten_state.permute(1, 0, 2)
        dialogue_flatten_action = dialogue_flatten_action.permute(1, 0, 2)
        dialogue_flatten_action_id = dialogue_flatten_action_id.permute(1, 0)
        # print(dialogue_flatten_state.size(), dialogue_flatten_action.size())
        return dialogue_flatten_state.to(self.device), dialogue_flatten_action.to(
            self.device), dialogue_flatten_action_id.to(self.device)