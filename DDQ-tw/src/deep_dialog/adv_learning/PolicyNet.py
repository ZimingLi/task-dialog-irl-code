import torch
import torch.nn as nn
import torch.nn.functional as F
import copy

class Policy_Net(nn.Module):
    def __init__(self, input_dim, action_num, rnn_tracker, dropout=0.0):
        # rnn_tracker
        super(Policy_Net, self).__init__()
        self.rnn_tracker = rnn_tracker
        interval = int((input_dim-action_num)/3)
        self.fc1 = nn.Linear(input_dim, input_dim-interval)
        self.drop1 = nn.Dropout(dropout)
        self.fc2 = nn.Linear(input_dim-interval, input_dim-2*interval)
        self.drop2 = nn.Dropout(dropout)
        self.fc3 = nn.Linear(input_dim-2*interval, action_num)

    def forward(self, input, current_hidden_state):
        # print("[]]]]]]")
        # print(input.size())
        _, hidden = self.rnn_tracker.forward(input_rnn_state_tracker=input, hidden=current_hidden_state)
        # hidden = hidden[-1]
        # print("policy",hidden.size())
        dout = nn.functional.relu(self.drop1(self.fc1(hidden[-1])))
        dout = nn.functional.relu(self.drop2(self.fc2(dout)))
        logit = self.fc3(dout)
        # output = F.softmax(logit, dim=1)
        distribution = F.log_softmax(logit, dim=1).data
        distribution = distribution.exp()
        return logit, distribution, hidden

    def next_action_sample(self, input, current_hidden_state):
        # logit: batch_size * action_num
        _, output, hidden = self.forward(input, current_hidden_state)
        distribution = output
        sampled_action_index = torch.multinomial(distribution, 1)[0]
        return sampled_action_index, distribution[0][sampled_action_index], hidden

    def next_action_greedy(self, input, current_hidden_state):
        # logit: batch_size * action_num
        logit, distribution, hidden = self.forward(input, current_hidden_state)
        _, index = torch.max(logit, 1)
        return index, distribution[0][index], hidden


class PG_Loss(nn.Module):
    def __init__(self):
        super(PG_Loss, self).__init__()
        self.log_softmax = nn.LogSoftmax(dim=1)

    def forward(self, logit_list, target, reward):
        """
        Inputs: pred, target, reward
            - pred: (batch_size, seq_len); pred=log_softmax(LSTM_output)
            - target : (batch_size, seq_len),
            - reward : (batch_size, ), reward of each whole sentence
        """
        one_hot = torch.zeros(logit_list.size(), dtype=torch.uint8)
        if logit_list.is_cuda:
            one_hot = one_hot.cuda()
        one_hot.scatter_(1, target.data.view(-1, 1), 1)
        # log(p)
        loss = torch.masked_select(logit_list, one_hot)
        # R * log(p)
        loss = loss * reward.contiguous().view(-1)
        loss = -torch.sum(loss)
        return loss

    def sequence_loss_by_example(self,logits, targets, mask, reward=None,):
        """
        :param logits: prediction of all generated sequence, seq_gen * batch_sie * action_num
        :param targets: ground truth, batch_size * seq_gen
        :param mask: batch_size * seq_gen
        :param reward: batch_size * seq_gen
        :return: cross_ent_loss, policy_reg_loss
        """
        # length = len(logits)
        # batch_size, action_num = logits[0].size()
        pred = torch.stack(logits)
        pred = pred.transpose(1,0)
        one_hot = torch.zeros(pred.size(), dtype=torch.uint8)
        if pred.is_cuda:
            one_hot = one_hot.cuda()
        one_hot.scatter_(2, targets.unsqueeze_(-1), 1)
        # log(p)
        loss = torch.masked_select(pred, one_hot)
        # log(p) +1
        ent_loss_1 = loss + 1
        ent_loss_2 = ent_loss_1.deatch()
        # if seq_len is fixed, mask==1
        # log(p) * (log(p) + 1) = -log(p) * (-log(p) - 1)
        ent_loss = ent_loss_2 * loss
        policy_ent_loss = ent_loss * mask

        # -R * log(p)
        crossent_loss = -loss * reward.contiguous().view(-1)
        crossent_loss = crossent_loss * mask
        crossent_loss = torch.sum(crossent_loss)

        return crossent_loss, policy_ent_loss

