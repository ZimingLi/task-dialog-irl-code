import json

import copy
import deep_dialog.agents.agent_adv as agent_adv
import deep_dialog.dialog_system.exchanger as exchanger
import deep_dialog.dialog_system.state_encoder as state_encoder
import deep_dialog.dialog_system.state_tracker as state_tracker
import deep_dialog.dialog_system.DataReader as datareader
import deep_dialog.dialog_config as dialog_config
import deep_dialog.usersims.usersim_rule as usersim_rule
from .discriminator import RewardModel
import numpy as np
import torch
import torch.optim as optim
import os
import torch.nn as nn
import random
import time


class DialogAdvTrain:
    """ A dialog manager to organize the training, including sampling, reward learning and training """

    def __init__(self, state_input_dim=20, action_embedding_dim=30, rnn_hidden_dim=100, use_cuda=None,
                 agent_params=None, user_params=None, act_set=None, slot_set=None, start_set=None,
                 movie_dictionary=None, max_turn=15, dropout=0.0, nlg_model=None, nlu_model=None,
                 movie_kb=None, device='cpu'):
        self.start_time = time.strftime("%Y%m%d-%H%M", time.localtime())
        self.act_set = act_set
        self.slot_set = slot_set
        self.start_set = start_set
        self.movie_dictionary = movie_dictionary
        self.max_turn = max_turn
        self.batch_size = agent_params['batch_size']
        self.beam_size = agent_params['beam_size']
        self.agent_params = agent_params
        self.act_cardinality = len(act_set.keys())
        self.slot_cardinality = len(slot_set.keys())
        self.feasible_actions = dialog_config.feasible_actions
        self.num_actions = len(self.feasible_actions)
        self.reward_training_times = 5
        self.lambda_factor = agent_params['lambda_factor']
        self.clip = 0.5
        self.teacher_forcing_flag = True
        self.rule_data_batch_num = agent_params['rule_data_size']
        self.device = device
        self.state_input_dim = 2 * self.act_cardinality + 4 * self.slot_cardinality + self.max_turn + dialog_config.kb_result_dim

        self.rnn_tracker_policy = state_encoder.State_Encoder(state_input_dim=self.state_input_dim,
                                                              action_embedding_dim=action_embedding_dim,
                                                              hidden_dim=rnn_hidden_dim,
                                                              use_cuda=use_cuda, n_layers=2, dropout=0).to(self.device)
        self.rnn_tracker_reward = state_encoder.State_Encoder(state_input_dim=self.state_input_dim,
                                                              action_embedding_dim=action_embedding_dim,
                                                              hidden_dim=rnn_hidden_dim,
                                                              use_cuda=use_cuda, n_layers=2, dropout=0).to(self.device)
        self.agent = agent_adv.AgentMLP(movie_kb=movie_kb,
                                        act_set=act_set,
                                        slot_set=slot_set,
                                        params=agent_params,
                                        policy_input_dim=rnn_hidden_dim,
                                        rnn_tracker=self.rnn_tracker_policy,
                                        device=self.device)
        self.user = usersim_rule.RuleSimulator(movie_dict=movie_dictionary,
                                               act_set=act_set,
                                               slot_set=slot_set,
                                               start_set=start_set,
                                               params=user_params)
        self.state_tracker = state_tracker.StateTracker(act_set=act_set,
                                                        slot_set=slot_set,
                                                        movie_kb=movie_dictionary,
                                                        max_turn=max_turn)
        self.agent.set_nlu_model(nlu_model)
        self.user.set_nlu_model(nlu_model)
        self.agent.set_nlg_model(nlg_model)
        self.user.set_nlg_model(nlg_model)
        self.exchanger = exchanger.DialogExchanger(agent=self.agent,
                                                   user=self.user,
                                                   act_set=act_set,
                                                   slot_set=slot_set,
                                                   movie_dictionary=movie_dictionary,
                                                   max_turn=self.max_turn)
        self.reward_model = RewardModel(self.num_actions, rnn_hidden_dim, dropout, self.rnn_tracker_reward).to(
            self.device)
        self.starting_epoch = 0
        if agent_params['trained_model_path'] != None:
            self.starting_epoch = self.load_model(agent_params['trained_model_path'])
            print("model loading finished")
        self.success_data_path = './deep_dialog/data/success_data.json'

    def sample_dialogue(self, print_act=False):
        # the first step: sample a batch of dialogues with the current policy net and rnn_state_tracker
        sampled_dialogue_list = []
        prob_list = []
        for _ in range(self.batch_size):
            traj, prob, _ = self.exchanger.sample_new_dialogue(sampling_bool=True, print_act=print_act,
                                                               learning_phase='train')
            sampled_dialogue_list.append(copy.deepcopy(traj))
            prob_list.append(prob)
        dialogue_list, sampled_mask = self.padding(sampled_dialogue_list)
        sampled_label = [-1] * len(dialogue_list)
        return dialogue_list, sampled_mask, sampled_label, np.array(prob_list)

    def generate_dialogue(self, print_act=False):
        # the first step: generate a batch of dialogues with the current policy net and rnn_state_tracker
        generate_dialogue_list = []
        prob_list = []
        num = 0
        while num < self.batch_size:
            traj, prob, vanilla_reward = self.exchanger.sample_new_dialogue(sampling_bool=False, print_act=print_act,
                                                                            learning_phase='train')
            generate_dialogue_list.append(copy.deepcopy(traj))
            prob_list.append(prob)
            num += 1
        dialogue_list, dialogue_mask = self.padding(generate_dialogue_list)
        dialogue_label = [-1] * len(dialogue_list)
        return dialogue_list, dialogue_mask, dialogue_label, np.array(prob_list)

    def testing_dialogue(self, print_act=True, test_num=500):
        # the first step: generate a batch of dialogues with the current policy net and rnn_state_tracker
        num = 0
        success_num = 0
        while num < test_num:
            traj, prob, vanilla_reward = self.exchanger.sample_new_dialogue(sampling_bool=False, print_act=print_act,
                                                                            learning_phase='test')
            if vanilla_reward > 0:
                print("dialogue ID: {},  Success ID: {}".format(num, success_num))
                success_num += 1
            else:
                print("dialogue ID: {},  Fail".format(num))
            num += 1
        print("========== Success rate: {}".format(success_num * 1.0 / test_num))

    def mc_search(self, dialogue_list):
        # estimate the reward for each position in each dialogue in one batch
        samples_state_input_list = []
        samples_action_target_list = []
        mc_reward_batch = []  # batch_size * decoding_steps
        mc_reward_before_t =[]
        dialogue_transpose = list(map(list, zip(*dialogue_list)))  # decoding_steps * batch_size
        pre_hidden_state = None
        for position_num, each_position in enumerate(dialogue_transpose):
            dialogue_history_before_t = copy.deepcopy(dialogue_transpose[:position_num + 1])
            dialogue_history_before_t = list(map(list, zip(*dialogue_history_before_t)))
            dialogue_sampled_after_t = []
            samples_state_input_list_t = []
            samples_action_target_list_t = []
            for d_num, each_dialogue_current_position in enumerate(each_position):
                # state = each_dialogue_current_position['state']
                dialogue_history_per_line = dialogue_history_before_t[d_num]
                hidden_state = each_dialogue_current_position['hidden_state']
                # action = each_dialogue_current_position['action']
                # action_id = self.action_index(action['act_slot_response'])
                action_id = each_dialogue_current_position['action_id']
                state_tracker_to_recover = each_dialogue_current_position['state_tracker_shot']
                user_state_to_recover = each_dialogue_current_position['running_user_shot']
                episode_over = each_dialogue_current_position['episode_over']
                # dialogue_prefix.append((state, hidden_state, action))
                # Todo: save state_input for rnn_tracker, and target action(sampled)
                samples_state_input_list_t.append(each_dialogue_current_position['state_rnn_input'])
                samples_action_target_list_t.append(action_id)
                for beam_num in range(self.beam_size):
                    dialogue_future_sampled = []
                    if position_num < self.max_turn / 2 - 1 and not episode_over:
                        ### we will not sample trajs from the last position or already finished positions
                        dialogue_future_sampled = self.exchanger.mc_search(
                            state_tracker_to_recover=copy.deepcopy(state_tracker_to_recover),
                            user_state_to_recover=copy.deepcopy(user_state_to_recover),
                            current_hidden_state=copy.deepcopy(hidden_state),
                            print_act=False)
                        dialogue_sampled_complete = dialogue_history_per_line + dialogue_future_sampled
                    else:
                        dialogue_sampled_complete = dialogue_history_per_line
                    # print(len(dialogue_history_per_line), len(dialogue_future_sampled), len(dialogue_sampled_complete))
                    dialogue_sampled_after_t.append(dialogue_sampled_complete)
            # padding sampled trajs and feed them to reward model
            dialogue_sampled_after_t, dialogue_sampled_after_t_mask = self.padding(dialogue_sampled_after_t)
            reward_current_position, pre_hidden_state, mc_reward_before_t = self.reward_estimate(dialogue_sampled_after_t,
                                                                             dialogue_sampled_after_t_mask,
                                                                             pre_hidden_state,
                                                                             position_num,
                                                                             mc_reward_before_t)  # batch_size * max_turn
            mc_reward_batch.append(reward_current_position)
            samples_state_input_list.append(torch.FloatTensor(samples_state_input_list_t))
            samples_action_target_list.append(torch.LongTensor(samples_action_target_list_t))
        return torch.stack(mc_reward_batch).to(self.device), torch.stack(samples_state_input_list).to(
            self.device), torch.stack(
            samples_action_target_list).to(self.device)

    def reward_estimate(self, dialogue_beam_sampled, dialogue_sampled_mask, hidden_state, position_num, mc_reward_before_t):
        """
        :param dialogue_beam_sampled: # (batch_size * beam) * max_turn
        :return:
        """
        turn_num = len(dialogue_beam_sampled[0])
        reward_mc = copy.deepcopy(mc_reward_before_t)  # max_turn * (batch * beam)
        dialogue_beam_sampled_tensor_state, dialogue_beam_sampled_tensor_action, _ = self.convert_dialogue_to_tensor(
            copy.deepcopy(dialogue_beam_sampled))
        # max_turn * (batch_size * beam_size) - swap dimension
        dialogue_sampled_mask_tensor = torch.FloatTensor(dialogue_sampled_mask).permute(1, 0).to(self.device)
        for i in range(position_num, turn_num):
            # current_position = dialogue_beam_sampled_tensor[i]
            reward, hidden_state = self.reward_model.forward(dialogue_beam_sampled_tensor_state[i],
                                                             dialogue_beam_sampled_tensor_action[i],
                                                             hidden_state)
            if i == position_num:
                new_hidden_state = copy.deepcopy(hidden_state)
            # print(i)
            # print("inside estimate",reward.size())
            mask = dialogue_sampled_mask_tensor[i].view(-1, 1)
            # print("inside estimate",mask.size())
            reward = torch.mul(reward, mask)
            reward_gathered = []
            # print("inside estimate",reward.size())
            for b_i in range(self.batch_size):
                reward_same_prefix = torch.mean(reward[b_i * self.beam_size:(b_i + 1) * self.beam_size])
                reward_gathered.append(reward_same_prefix.clone())
            reward_mc.append(torch.stack(reward_gathered))
            if i == position_num:
                reward_first_position = copy.deepcopy(torch.stack(reward_gathered))
            # reward: (batch_size * beam_size)
        # reward_mc (max_turn * batch_size)
        mc_reward_before_t.append(reward_first_position)
        return torch.stack(reward_mc), new_hidden_state, mc_reward_before_t

    def convert_dialogue_to_tensor(self, dialogue_beam_sampled):
        """
        :param dialogue_beam_sampled: (batch_size * beam_size) * max_turn
        :return: max_turn * (batch_size * beam_size)
        """
        dialogue_flatten_state = []
        dialogue_flatten_action = []
        dialogue_flatten_action_id = []
        # print(len(dialogue_beam_sampled), len(dialogue_beam_sampled[0]), len(dialogue_beam_sampled[1][0]))
        for dialog_l in dialogue_beam_sampled:
            full_dialog_state = []
            full_dialog_action = []
            full_dialog_action_id = []
            for each_sa in dialog_l:
                # print(each_sa)
                state = each_sa['state_rnn_input']
                action = self.action_index(each_sa['action'])
                # action = action.ravel().tolist()
                action_id = each_sa['action_id']
                state = torch.FloatTensor(state)
                # this is the ine-hot representation of predicted action
                action = torch.FloatTensor(action)
                # this is the index of predicted action
                # action_id = torch.LongTensor(action_id)
                full_dialog_state.append(copy.deepcopy(state))
                full_dialog_action.append(copy.deepcopy(action))
                # this is the index of predicted action
                full_dialog_action_id.append(copy.deepcopy(action_id))
            full_dialog_state = torch.stack(full_dialog_state)
            full_dialog_action = torch.stack(full_dialog_action)
            full_dialog_action_id = torch.LongTensor(full_dialog_action_id)
            dialogue_flatten_state.append(full_dialog_state)
            dialogue_flatten_action.append(full_dialog_action)
            dialogue_flatten_action_id.append(full_dialog_action_id)

        dialogue_flatten_state = torch.stack(dialogue_flatten_state)
        dialogue_flatten_action = torch.stack(dialogue_flatten_action)
        dialogue_flatten_action_id = torch.stack(dialogue_flatten_action_id)

        # max_turn * (batch_size * beam_size)
        dialogue_flatten_state = dialogue_flatten_state.permute(1, 0, 2)
        dialogue_flatten_action = dialogue_flatten_action.permute(1, 0, 2)
        dialogue_flatten_action_id = dialogue_flatten_action_id.permute(1, 0)
        # print(dialogue_flatten_state.size(), dialogue_flatten_action.size())
        return dialogue_flatten_state.to(self.device), dialogue_flatten_action.to(
            self.device), dialogue_flatten_action_id.to(self.device)

    def extract_action_to_one_hot(self, action):
        # extract action_id from next state
        # Encode last agent act
        agent_last = action['act_slot_response']
        agent_act_rep = np.zeros((1, self.act_cardinality))
        if agent_last:
            agent_act_rep[0, self.act_set[agent_last['diaact']]] = 1.0
        agent_inform_slots_rep = np.zeros((1, self.slot_cardinality))
        if agent_last:
            for slot in agent_last['inform_slots'].keys():
                agent_inform_slots_rep[0, self.slot_set[slot]] = 1.0
        agent_request_slots_rep = np.zeros((1, self.slot_cardinality))
        if agent_last:
            for slot in agent_last['request_slots'].keys():
                agent_request_slots_rep[0, self.slot_set[slot]] = 1.0
        return agent_act_rep.ravel().tolist()

    # TODO: check the dim of action

    def action_index(self, action):
        """ Return the index of action """
        agent_slot_response = action['act_slot_response']
        agent_rep = np.zeros((1, self.num_actions))
        for (i, action_each) in enumerate(self.feasible_actions):
            if agent_slot_response == action_each:
                agent_rep[0][i] = 1.0
        return agent_rep.ravel().tolist()

    def padding(self, dialogues_to_be_padded):
        """
        :param dialogues_to_be_padded: batch_size * beam * turns (not the same length)
        :return: dialogues_padded: batch_size * max_turn
        """
        dialogues_padded = []
        mask = []
        for each_line in dialogues_to_be_padded:
            length = len(each_line)
            padded_length = self.max_turn / 2 - length
            new_line = copy.deepcopy(each_line)
            # last_shot = copy.deepcopy(each_line[-1])
            for num in range(padded_length):
                last_shot = copy.deepcopy(each_line[-1])
                last_shot['state_tracker_shot']['turn_count'] = min(
                    last_shot['state_tracker_shot']['turn_count'] + 2 * (num + 1), self.max_turn)
                new_line.append(last_shot)
            mask_line = [1] * length + [0] * padded_length
            dialogues_padded.append(new_line)
            mask.append(mask_line)
            # print(len(new_line))
        return dialogues_padded, mask

    def generate_rule_data(self, batch_num):
        rule_data = []
        self.agent.rule_data = True

        def generate_dialogue_batch():
            # the first step: generate a batch of dialogues with the current policy net and rnn_state_tracker
            generate_dialogue_list = []
            num = 0
            while num < self.batch_size:
                traj, _, _ = self.exchanger.sample_new_dialogue(sampling_bool=False, print_act=True)
                if traj[-1]['dialogue_status'] == 1:
                    generate_dialogue_list.append(traj)
                    num += 1
            dialogue_list, dialogue_mask = self.padding(generate_dialogue_list)
            dialogue_label = [1] * len(dialogue_list)
            return dialogue_list, dialogue_mask, dialogue_label

        for _ in range(batch_num):
            dialog = generate_dialogue_batch()
            rule_data.append(dialog)
        self.agent.rule_data = False
        return rule_data

    def importance_weight(self, dialogue_sampled, dialogue_sampled_mask, dialogue_sampled_prob):
        importance_weight = []
        turn_num = len(dialogue_sampled[0])
        dialog_num = len(dialogue_sampled)
        reward_mc = []  # max_turn * (batch * beam)
        dialogue_beam_sampled_tensor_state, dialogue_beam_sampled_tensor_action, _ = self.convert_dialogue_to_tensor(
            copy.deepcopy(dialogue_sampled))
        # max_turn * (batch_size * beam_size) - swap dimension
        dialogue_sampled_mask_tensor = torch.FloatTensor(dialogue_sampled_mask).permute(1, 0).to(self.device)
        hidden_state = None
        reward_position_list = []
        for i in range(turn_num):
            # current_position = dialogue_beam_sampled_tensor[i]
            reward, hidden_state = self.reward_model.forward(dialogue_beam_sampled_tensor_state[i],
                                                             dialogue_beam_sampled_tensor_action[i],
                                                             hidden_state)
            mask = dialogue_sampled_mask_tensor[i].view(-1, 1)
            reward = torch.mul(reward, mask).squeeze(1)
            reward_position_list.append(reward)
        reward_sampled = torch.stack(reward_position_list)
        reward_list = torch.sum(reward_sampled, dim=0)
        for dialog_id in range(dialog_num):
            reward_diff = reward_list - reward_list[dialog_id]
            reward_diff = torch.exp(reward_diff).cpu().numpy()
            prob_diff = 1.0 * dialogue_sampled_prob[dialog_id] / dialogue_sampled_prob
            product = 1.0 / np.sum(reward_diff * prob_diff)
            importance_weight.append(product)
        return importance_weight

    def teacher_forcing(self, human_dialogue, human_dialogue_mask):
        state, _, action_id = self.convert_dialogue_to_tensor(human_dialogue)
        turn_num = len(human_dialogue_mask[0])
        reward_fake_single = copy.deepcopy(human_dialogue_mask)
        reward_fake = [reward_fake_single] * turn_num
        reward_fake = torch.FloatTensor(reward_fake)
        if len(reward_fake.shape) == 3:
            reward_fake = reward_fake.permute(0, 2, 1)
        else:
            reward_fake = reward_fake.transpose()
        return state, action_id, reward_fake

        # TODO: teacher forcing: 1. convert to tensor, 2. get the reward using mask matrix

    def adv_training(self):
        agent_optimizer = optim.Adam(self.agent.policy_net.parameters(), lr=0.005, betas=(0.9, 0.99))
        reward_optimizer = optim.Adam(self.reward_model.parameters(), lr=0.005, betas=(0.9, 0.99))
        # rule_data = self.generate_rule_data(self.rule_data_batch_num)
        success_data = datareader.DataReader(self.success_data_path, self.act_set, self.slot_set, self.max_turn,
                                             self.device)
        epoch = self.starting_epoch
        ending_epoch = self.starting_epoch + 3000
        while epoch < ending_epoch:
            agent_optimizer.zero_grad()
            # agent_state_encoder_optimizer.zero_grad()
            # reward_optimizer.zero_grad()
            print("========= Agent Training  ========================")
            # step1: sampling
            with torch.no_grad():
                print("sampling")
                sampled_dialogues, sampled_dialogues_mask, sampled_dialogues_label, _ = self.sample_dialogue()
                # step2: rewarding
                print("rewarding")
                mc_rewarding, state_input, action_sampled = self.mc_search(copy.deepcopy(sampled_dialogues))
            # step3: training
            print("agent training")
            agent_loss_cross, agent_loss_reg = self.agent.train(state_tracker_input=state_input.to(self.device),
                                                                target_action=action_sampled.to(self.device),
                                                                reward=mc_rewarding.to(self.device),
                                                                mask=torch.FloatTensor(sampled_dialogues_mask).to(
                                                                    self.device))
            agent_loss = agent_loss_cross + agent_loss_reg * self.lambda_factor
            agent_loss.backward()
            _ = torch.nn.utils.clip_grad_norm_(self.agent.policy_net.parameters(), self.clip)
            agent_optimizer.step()

            print(self.num_actions)
            if self.teacher_forcing_flag:
                print("========= Teacher Forcing  ========================")
                agent_optimizer.zero_grad()
                # rule_data_index = random.randint(0, self.rule_data_batch_num - 1)
                # human_dialogue, human_dialogue_mask, human_dialogue_label = rule_data[rule_data_index]
                human_dialogue, human_dialogue_mask, human_dialogue_label = success_data.sample_batch(self.batch_size)
                teacher_input, teacher_target, teacher_reward = self.teacher_forcing(human_dialogue,
                                                                                     human_dialogue_mask)
                teacher_loss_cross, teacher_loss_reg = self.agent.train(
                    state_tracker_input=teacher_input.to(self.device),
                    target_action=teacher_target.to(self.device),
                    reward=teacher_reward.to(self.device),
                    mask=torch.FloatTensor(human_dialogue_mask).to(
                        self.device))
                # teacher_loss = teacher_loss_cross
                teacher_loss_cross.backward()
                _ = torch.nn.utils.clip_grad_norm_(self.agent.policy_net.parameters(), self.clip)
                agent_optimizer.step()
            print("====reward training====")
            # ========= Reward Training  ========================
            for _ in range(self.reward_training_times):
                reward_optimizer.zero_grad()
                # rule_data_index = random.randint(0, self.rule_data_batch_num - 1)
                # human_dialogue, human_dialogue_mask, human_dialogue_label = rule_data[rule_data_index]
                human_dialogue, human_dialogue_mask, human_dialogue_label = success_data.sample_batch(self.batch_size)
                with torch.no_grad():
                    # TODO: replace sampled dialogues with greedy dialogues
                    # machine_dialogues, machine_dialogues_mask, machine_dialogues_label, machine_dialogue_prob = self.sample_dialogue()
                    machine_dialogues, machine_dialogues_mask, machine_dialogues_label, machine_dialogue_prob = self.generate_dialogue()
                    importance_weight = self.importance_weight(machine_dialogues, machine_dialogues_mask,
                                                               machine_dialogue_prob)
                    importance_weight = -np.array(importance_weight)
                    machine_dialogues_label = importance_weight.tolist()
                # concatenate human dialogues and machine dialogues
                mix_data = machine_dialogues + human_dialogue
                mix_mask = machine_dialogues_mask + human_dialogue_mask
                mix_label = machine_dialogues_label + human_dialogue_label
                # max_turn * batch_size
                mix_data_tensor = self.convert_dialogue_to_tensor(copy.deepcopy(mix_data))
                reward_loss = self.reward_model.train(mix_data_tensor,
                                                      torch.FloatTensor(mix_mask).to(self.device),
                                                      torch.FloatTensor(mix_label).to(self.device))
                reward_loss.backward()
                _ = torch.nn.utils.clip_grad_norm_(self.reward_model.parameters(), self.clip)
                reward_optimizer.step()
            print("======== Epoch {} finished ===========".format(epoch))
            if epoch % 25 == 0:
                self.testing_dialogue(False, 500)
                self.save_model(self.agent_params['model_path'], epoch)
                # _, _, _, _ = self.generate_dialogue(print_act=True)
            epoch += 1

    """ Save model """

    def save_model(self, path, cur_epoch):
        filename = 'adv_{}_lambda_{}_device_{}_time_{}.pt'.format(cur_epoch, self.lambda_factor, self.device,
                                                                  self.start_time)
        filepath = os.path.join(path, filename)
        torch.save({
            'epoch': cur_epoch,
            'agent_state_dict': self.agent.policy_net.state_dict(),
            'reward_state_dict': self.reward_model.state_dict(),
        }, filepath)

    def load_model(self, path):
        checkpoint = torch.load(path)
        self.agent.policy_net.load_state_dict(checkpoint['agent_state_dict'])
        self.reward_model.load_state_dict(checkpoint['reward_state_dict'])
        epoch = checkpoint['epoch']
        return epoch
