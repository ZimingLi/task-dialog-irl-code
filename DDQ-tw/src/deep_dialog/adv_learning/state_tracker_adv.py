import torch
import torch.nn as nn
import torch.nn.functional as F

class MLP(nn.Module):
    def __init__(self, input_dim, output_dim, dropout=0.0):
        super(MLP, self).__init__()
        interval = (input_dim-output_dim)/3
        self.fc1 = nn.Linear(input_dim, input_dim-interval)
        self.drop1 = nn.Dropout(dropout)
        self.fc2 = nn.Linear(input_dim-interval, input_dim-2*interval)
        self.drop2 = nn.Dropout(dropout)
        self.fc3 = nn.Linear(input_dim-2*interval, output_dim)

    def forward(self, input):
        dout = nn.functional.relu(self.drop1(self.fc1(input)))
        dout = nn.functional.relu(self.drop2(self.fc2(dout)))
        return nn.functional.softmax(self.fc3(dout))

class Belief_Tracker(nn.Module):
    def __init__(self, state_dim, slot_num, value_type_list, dropout=0.0):
        super(Belief_Tracker,self).__init__()
        self.mlp_list=[]
        for slot_value in value_type_list:
            mlp = self.create_MLP(state_dim, slot_value, dropout)
            self.mlp_list.append(mlp)

    def create_MLP(self, input_dimen, output_dimen, dropout):
        return MLP(input_dimen, output_dimen, dropout)

    def forward(self, state_input):
        state_output_list = []
        for slot_mlp in self.mlp_list:
            state_output=slot_mlp.forward(state_input)
            state_output_list.append(state_output)
        return state_output_list

def retrieve_from_KB(slots, value_dist):
    constraint = []
    for slot_name, slot_value in enumerate(slots, value_dist):
        _, index = torch.max(slot_value)
        constraint.append((slot_name, index))
    return API_call(constraint)

def API_call(constraint):
    pass


