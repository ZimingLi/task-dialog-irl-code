import torch
import torch.nn as nn
import torch.nn.functional as F
import copy
import numpy as np
import torch.optim as optim


class RewardModel(nn.Module):
    """
    A CNN for text classification.
    Uses an embedding layer, followed by a convolutional, max-pooling and softmax layer.
    Highway architecture based on the pooled feature maps is added. Dropout is adopted.
    """

    def __init__(self, action_dim, state_hidden_dim, dropout_prob, rnn_tracker):
        super(RewardModel, self).__init__()
        self.dropout = nn.Dropout(p=dropout_prob)
        mlp_dim = int(state_hidden_dim / 2) + int(action_dim / 2)
        interval = int(mlp_dim / 3)
        self.fc_m1 = nn.Linear(mlp_dim, mlp_dim - interval)
        self.drop_m1 = nn.Dropout(dropout_prob)
        self.fc_m2 = nn.Linear(mlp_dim - interval, mlp_dim - 2 * interval)
        self.drop_m2 = nn.Dropout(dropout_prob)
        self.fc_m3 = nn.Linear(mlp_dim - 2 * interval, 1)
        self.rnn_tracker = rnn_tracker
        self.sigmoid = nn.Sigmoid()

        self.fc1 = nn.Linear(state_hidden_dim, int(state_hidden_dim / 2))
        self.drop1 = nn.Dropout(dropout_prob)
        self.fc2 = nn.Linear(action_dim, int(action_dim / 2))
        self.drop2 = nn.Dropout(dropout_prob)

    def forward(self, state_input, action_input, current_hidden_state):
        output, hidden = self.rnn_tracker.forward(input_rnn_state_tracker=state_input, hidden=current_hidden_state)
        # hidden_final = hidden[-1].clone()
        state_representation = nn.functional.relu(self.drop1(self.fc1(hidden[-1])))
        action_representation = nn.functional.relu(self.drop2(self.fc2(action_input)))
        # print(hidden.size(),state_representation.size(), action_representation.size())
        final = torch.cat((state_representation, action_representation), 1)
        mlp1 = nn.functional.relu(self.drop_m1(self.fc_m1(final)))
        mlp2 = nn.functional.relu(self.drop_m2(self.fc_m2(mlp1)))
        reward = self.sigmoid(self.fc_m3(mlp2)) - 0.5
        return reward, hidden

    def train(self, dialogues, dialogues_mask, weight):
        """
        :param dialogue_beam_sampled: batch_size * turn_num (half positive, half negative)
        :param dialogue_sampled_mask: batch_size * turn_num
        :param weight: batch_size (half positive, half negative)
        :return: loss
        """
        # print("reward training")
        loss, _ = self.reward_estimate(dialogues, dialogues_mask, weight, True)
        return -loss

    def reward_estimate(self, dialogues, dialogues_mask, weight, print_bool):
        """
        :param dialogue_beam_sampled: [state: turn_num * batch_size, action: turn_num * batch_size, action_id: turn_num * batch_size]
        :param dialogue_sampled_mask:  batch_size * turn_num
        :param weight: batch_size, this is the weight or label of input dialogues
        Todo: replace weight with importance sampling weight
        :return:
        """
        state, action, _ = dialogues
        turn_num = len(state)
        reward_all = []  # max_turn * (batch * beam)
        # dialogue_beam_sampled_tensor = dialogues
        # max_turn * batch
        dialogue_sampled_mask_tensor = dialogues_mask.permute(1,0)
        dialogue_sampled_weight_tensor = weight.view(1, -1)
        hidden_state = None
        for i in range(turn_num):
            reward, hidden_state = self.forward(state[i], action[i], hidden_state)
            reward = torch.mul(reward, dialogue_sampled_mask_tensor[i].view(-1,1))
            # reward = torch.mul(reward, dialogue_sampled_weight_tensor)
            reward_all.append(reward)
            # print(reward.data)
        reward_all_original = torch.stack(reward_all)
        reward_all = reward_all_original.sum(dim=0).view(1,-1)  # 1 * batch_size
        if print_bool:
            print(reward_all.data)
        reward_all = reward_all * dialogue_sampled_weight_tensor
        reward_finall = reward_all.mean()
        return reward_finall, reward_all_original.permute(1,0,2)

    # def forward_multiple(self, ):

    def get_state(self, x):
        """
        Inputs: x
            - x: (batch_size, seq_len)
        Outputs: out
            - out: (batch_size, num_classes)
        """
        emb = self.embed(x).unsqueeze(1)  # batch_size, 1 * seq_len * emb_dim
        convs = [F.relu(conv(emb)).squeeze(3) for conv in self.convs]  # [batch_size * num_filter * seq_len]
        pools = [F.max_pool1d(conv, conv.size(2)).squeeze(2) for conv in convs]  # [batch_size * num_filter]
        out = torch.cat(pools, 1)  # batch_size * sum(num_filters)
        highway = self.highway(out)
        transform = F.sigmoid(highway)
        out = transform * F.relu(highway) + (1. - transform) * out  # sets C = 1 - T
        out = F.log_softmax(self.fc(self.dropout(out)), dim=1)  # batch * num_classes
        return out

    def get_reward(self, dialog_seq):
        pass

    def update_disc(self):
        pass

class DiscModel(nn.Module):
    """
    A CNN for text classification.
    Uses an embedding layer, followed by a convolutional, max-pooling and softmax layer.
    Highway architecture based on the pooled feature maps is added. Dropout is adopted.
    """

    def __init__(self, action_dim, state_input_dim, dropout_prob, state_hidden_dim, batch_size, device):
        super(DiscModel, self).__init__()
        self.dropout = nn.Dropout(p=dropout_prob)

        self.input_dim = state_input_dim
        self.hidden_dim = state_hidden_dim
        self.output_dim = 2
        self.num_layers = 1
        self.batch_size = batch_size
        self.device = torch.device(device)
        self.input_size = state_input_dim - (state_input_dim - state_hidden_dim)/2
        self.bilstm = nn.LSTM(input_size=self.input_size, hidden_size=state_hidden_dim,
                              num_layers=self.num_layers, batch_first=False,
                              dropout=dropout_prob, bidirectional=True)
        self.mean = 'mean'
        self.layer1 = nn.Linear(state_input_dim, self.input_size)
        self.layer2 = nn.Linear(state_hidden_dim * 2, self.output_dim)

        self.loss_function = nn.NLLLoss()


    def init_hidden(self, batch_size):
        return (torch.zeros(self.num_layers * 2, batch_size, self.hidden_dim).to(self.device),
                torch.zeros(self.num_layers * 2, batch_size, self.hidden_dim).to(self.device))


    def forward(self, sentence):
        # sentence: batch_size * len * dim
        sent_proj = self.layer1(sentence)
        x = sent_proj.permute(1, 0, 2)
        # we do this because the default parameter of lstm is False; len * batch_size * dim
        self.hidden = self.init_hidden(sentence.size()[0])
        # 2 * batch_size * dim
        lstm_out, self.hidden = self.bilstm(x, self.hidden)
        # lstm_out:len * batch_size * (dim *2)
        if self.mean == "mean":
            out = lstm_out.permute(1, 0, 2)
            final = torch.mean(out, 1)
        else:
            final = lstm_out[-1]
        # batch_size * (dim *2)
        score = self.layer2(final)
        log_soft_score = F.log_softmax(score, dim=1)
        soft_score = F.softmax(score, dim=1)
        # batch_size * 2  #lstm_out[-1]
        return log_soft_score, soft_score

    def train(self, dialogues, label):
        '''
        :param dialogues: tensor(batch_size * len * dim)
        :param label:
        :return:
        '''
        # print("reward training")
        scores, _ = self.forward(dialogues)
        loss = self.loss_function(scores, label)
        return loss

    def get_reward(self, dialog_seq):
        return self.forward(dialog_seq)


