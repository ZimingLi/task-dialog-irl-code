import torch
import copy
import numpy as np
import json
import deep_dialog.dialog_config as dialog_config

class DataReader():

    def __init__(self, path, act_set, slot_set, max_turn, device):
        self.act_set = act_set
        self.slot_set = slot_set
        self.act_cardinality = len(act_set.keys())
        self.slot_cardinality = len(slot_set.keys())
        self.device = device
        # self.feasible_actions = dialog_config.feasible_actions
        self.feasible_actions = dialog_config.feasible_actions
        self.num_actions = len(self.feasible_actions)

        file_targ = open(path, 'r')
        data = json.load(file_targ)
        self.num = len(data)
        self.max_turn = max_turn
        self.data, self.mask = self.preprocess_data(data)

        self.label = [1] * self.num

    def preprocess_data(self, data):
        data_padded, mask = self.padding(data)
        # data_tensor = self.convert_dialogue_to_tensor(data_padded)
        return data_padded, mask

    def padding(self, data):
        data_padded = []
        mask = []
        for each_diag in data:
            length = len(each_diag)
            padded_length = self.max_turn/2 - length
            last_turn = each_diag[-1]
            new_dialog = copy.deepcopy(each_diag)
            for _ in range(padded_length):
                new_dialog.append(copy.deepcopy(last_turn))
            data_padded.append(new_dialog)
            mask_line = [1] * length + [0] * padded_length
            mask.append(mask_line)
        return data_padded, mask

    def convert_dialogue_to_tensor(self, dialogues):
        """
        :param dialogue_beam_sampled: (batch_size * beam_size) * max_turn
        :return: max_turn * (batch_size * beam_size)
        """
        dialogue_flatten_state = []
        dialogue_flatten_action = []
        dialogue_flatten_action_id = []
        # print(len(dialogue_beam_sampled), len(dialogue_beam_sampled[0]), len(dialogue_beam_sampled[1][0]))
        for dialog_l in dialogues:
            full_dialog_state = []
            full_dialog_action = []
            full_dialog_action_id = []
            for each_sa in dialog_l:
                # print(each_sa)
                state = each_sa['state_rnn_input']
                action = self.action_index(each_sa['action'])
                # action = action.ravel().tolist()
                action_id = each_sa['action_id']
                state = torch.FloatTensor(state)
                # this is the one-hot representation of predicted action
                action = torch.FloatTensor(action)
                # this is the index of predicted action
                # action_id = torch.LongTensor(action_id)
                full_dialog_state.append(copy.deepcopy(state))
                full_dialog_action.append(copy.deepcopy(action))
                # this is the index of predicted action
                full_dialog_action_id.append(copy.deepcopy(action_id))
            full_dialog_state = torch.stack(full_dialog_state)
            full_dialog_action = torch.stack(full_dialog_action)
            full_dialog_action_id = torch.LongTensor(full_dialog_action_id)
            dialogue_flatten_state.append(full_dialog_state)
            dialogue_flatten_action.append(full_dialog_action)
            dialogue_flatten_action_id.append(full_dialog_action_id)

        dialogue_flatten_state = torch.stack(dialogue_flatten_state)
        dialogue_flatten_action = torch.stack(dialogue_flatten_action)
        dialogue_flatten_action_id = torch.stack(dialogue_flatten_action_id)

        # max_turn * (batch_size * beam_size)
        dialogue_flatten_state = dialogue_flatten_state.permute(1, 0, 2)
        dialogue_flatten_action = dialogue_flatten_action.permute(1, 0, 2)
        dialogue_flatten_action_id = dialogue_flatten_action_id.permute(1, 0)
        # print(dialogue_flatten_state.size(), dialogue_flatten_action.size())
        return dialogue_flatten_state.to(self.device), dialogue_flatten_action.to(
            self.device), dialogue_flatten_action_id.to(self.device)

    def action_index(self, action):
        """ Return the index of action """
        agent_slot_response = action['act_slot_response']
        agent_rep = np.zeros((1, self.num_actions))
        for (i, action_each) in enumerate(self.feasible_actions):
            if agent_slot_response == action_each:
                agent_rep[0][i] = 1.0
        return agent_rep.ravel().tolist()

    def action_to_one_hot(self, action):
        # extract action_id from next state
        # Encode last agent act
        agent_last = action['act_slot_response']
        agent_act_rep = np.zeros((1, self.act_cardinality))
        if agent_last:
            agent_act_rep[0, self.act_set[agent_last['diaact']]] = 1.0
        agent_inform_slots_rep = np.zeros((1, self.slot_cardinality))
        if agent_last:
            for slot in agent_last['inform_slots'].keys():
                agent_inform_slots_rep[0, self.slot_set[slot]] = 1.0
        agent_request_slots_rep = np.zeros((1, self.slot_cardinality))
        if agent_last:
            for slot in agent_last['request_slots'].keys():
                agent_request_slots_rep[0, self.slot_set[slot]] = 1.0
        return agent_act_rep.ravel().tolist()

    def sample_batch(self, batch_size):
        data_batch = []
        mask_batch = []
        label_batch = []
        sampled_list = np.random.choice(self.num, batch_size, replace=False)
        for index in sampled_list:
            data_batch.append(self.data[index])
            mask_batch.append(self.mask[index])
            label_batch.append(self.label[index])
        return data_batch, mask_batch, label_batch


class DiscDataReader():

    def __init__(self, path, act_set, slot_set, max_turn, discdata_processor, device):
        self.act_set = act_set
        self.slot_set = slot_set
        self.act_cardinality = len(act_set.keys())
        self.slot_cardinality = len(slot_set.keys())
        self.device = device
        # self.feasible_actions = dialog_config.feasible_actions
        self.feasible_actions = dialog_config.feasible_actions
        self.num_actions = len(self.feasible_actions)
        self.discdata_processor = discdata_processor

        file_targ = open(path, 'r')
        data = json.load(file_targ)
        self.num = len(data)
        print("success data num: ", self.num)
        self.max_turn = max_turn
        self.data = self.preprocess_data(data)
        self.label = [1] * self.num
        self.raw_data, self.mask = self.prepocess_data_for_tf(data)

    def preprocess_data(self, data):
        data_padded, mask = self.discdata_processor.padding(data)
        data_for_classify = self.discdata_processor.prepare_input_for_disc(data_padded)
        return data_for_classify

    def prepocess_data_for_tf(self, data):
        data_padded, mask = self.discdata_processor.padding(data)
        return data_padded, mask


    def action_index(self, action):
        """ Return the index of action """
        agent_slot_response = action['act_slot_response']
        agent_rep = np.zeros((1, self.num_actions))
        for (i, action_each) in enumerate(self.feasible_actions):
            if agent_slot_response == action_each:
                agent_rep[0][i] = 1.0
        return agent_rep.ravel().tolist()


    def sample_batch(self, batch_size):
        data_batch = []
        label_batch = []
        sampled_list = np.random.choice(self.num, batch_size, replace=False)
        for index in sampled_list:
            data_batch.append(self.data[index])
            label_batch.append(self.label[index])
        return torch.stack(data_batch), label_batch

    def sample_batch_for_tf(self, batch_size):
        data_batch = []
        label_batch = []
        mask_batch = []
        sampled_list = np.random.choice(self.num, batch_size, replace=False)
        for index in sampled_list:
            data_batch.append(self.raw_data[index])
            label_batch.append(self.label[index])
            mask_batch.append(self.mask[index])

        return data_batch, mask_batch, label_batch
