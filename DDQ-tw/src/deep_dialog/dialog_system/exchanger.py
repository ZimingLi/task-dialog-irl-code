import json
from . import StateTracker
from .. import dialog_config
import copy
import torch

class DialogExchanger:
    """ A dialog manager to mediate the interaction between an agent and a customer """

    def __init__(self, agent, user, act_set, slot_set, movie_dictionary, max_turn):
        self.agent = agent
        self.user = user
        self.act_set = act_set
        self.slot_set = slot_set
        self.reward = 0
        self.episode_over = False
        self.movie_dictionary = movie_dictionary
        self.use_world_model = False
        self.running_user = self.user
        self.state_action_list = []
        self.max_turn = max_turn

    def initialize_episode(self, print_act=False, learning_phase='train'):
        """ Refresh state for new dialog """

        self.reward = 0
        self.episode_over = False
        # initialize a state tracker
        self.state_tracker = StateTracker(self.act_set, self.slot_set, self.movie_dictionary, self.max_turn)
        self.state_tracker.initialize_episode()
        self.use_world_model = False
        self.state_action_list = []
        # initialize a new user
        self.running_user = self.user
        self.use_world_model = False
        user_action = self.running_user.initialize_episode(print_act, learning_phase)
        # save the screenshot of user
        self.state_tracker.update(user_action=user_action)
        if print_act:
            self.print_function(user_action=user_action)

        self.agent.initialize_episode()
        return self.state_tracker, user_action

    def next_turn(self, print_act=False, current_hidden_state=None, sampling_bool=True):
        """
        :param record_training_data:
        :param current_hidden_state: feed the state at last time step
        :return:
        """
        """ This function initiates each subsequent exchange between agent and user (agent first) """
        # TODO: save the state input, hidden state of state_tracker, the selected action (Done)
        # TODO: be careful of the hidden state-> how to save it to a list? clone()?
        # We use the hidden state of state_tracker as our reward model input
        ########################################################################
        #   CALL AGENT TO TAKE HER TURN
        ########################################################################
        # we save two kinds of state representation, one is slot based which is from the state tracker, another
        # one is exactly the hidden state of RNN_state_tracker; we will try both methods to build the reward model
        self.state = self.state_tracker.get_state_for_agent()  # store this state
        self.state_rnn_input = self.state_tracker.get_input_for_rnn_state_tracker()
        # here we input the concatenated vector as state_rnn's input; the previous hidden state is also fed to RNN
        # we receive the sampled action and current hidden state from RNN
        self.agent_action, self.action_id, current_hidden_state = self.agent.state_to_action(torch.FloatTensor(self.state_rnn_input).view(1, -1), current_hidden_state, sampling_bool)
        action_raw=copy.deepcopy(self.agent_action)
        # save the current_hidden_state as another representation of state
        ########################################################################
        #   Register AGENT action with the state_tracker
        ########################################################################
        self.state_tracker.update(agent_action=self.agent_action)
        self.state_user = self.state_tracker.get_state_for_user()

        self.agent.add_nl_to_action(self.agent_action)  # add NL to Agent Dia_Act
        if print_act:
            self.print_function(agent_action=self.agent_action['act_slot_response'])


        ########################################################################
        #   CALL USER TO TAKE HER TURN
        ########################################################################
        self.sys_action = self.state_tracker.dialog_history_dictionaries()[-1]
        self.user_action, self.episode_over, dialog_status = self.running_user.next(self.sys_action)
        self.reward = self.reward_function(dialog_status)
        ########################################################################
        #   Update state tracker with latest user action
        ########################################################################

        if not sampling_bool and self.episode_over != True and self.state_tracker.turn_count < self.max_turn:
            self.state_tracker.update(user_action=self.user_action)
            if print_act:
                self.print_function(user_action=self.user_action)
        elif sampling_bool and self.state_tracker.turn_count < self.max_turn:
            self.state_tracker.update(user_action=self.user_action)
            if print_act:
                self.print_function(user_action=self.user_action)

        self.state_user_next = self.state_tracker.get_state_for_agent()

        # return the screenshot of state tracker, state s, action a, next_state s', the screentshot of user simulator
        turn_shot = {}

        turn_shot['state_tracker_shot'] = self.state_tracker.save_state_tracker_screenshot()
        turn_shot['running_user_shot'] = self.running_user.save_user_state_and_goal()
        turn_shot['state_rnn_input'] = self.state_rnn_input
        turn_shot['action'] = action_raw
        turn_shot['action_final'] = copy.deepcopy(self.agent_action)
        turn_shot['action_id'] = self.action_id
        turn_shot['user_reply'] = copy.deepcopy(self.user_action)
        turn_shot['next_state'] = self.state_tracker.get_state_for_agent()
        turn_shot['episode_over'] = self.episode_over
        turn_shot['dialogue_status'] = dialog_status
        turn_shot['vanilla_reward'] = self.reward
        turn_shot['hidden_state'] = current_hidden_state
        return turn_shot

    def reward_function(self, dialog_status):
        """ Reward Function 1: a reward function based on the dialog_status """
        if dialog_status == dialog_config.FAILED_DIALOG:
            reward = -self.user.max_turn  # 10
        elif dialog_status == dialog_config.SUCCESS_DIALOG:
            reward = 2 * self.user.max_turn  # 20
        else:
            reward = -1
        return reward

    def sample_new_dialogue(self, sampling_bool=True, print_act=False, learning_phase='train'):
        """
        sample a dialogue between the sys agent and user environment
        :return: a complete dialogue
        """
        self.episode_over = False
        self.state_tracker, user_action = self.initialize_episode(print_act=print_act, learning_phase=learning_phase)
        dialogue_traj = []
        current_hidden_state = None
        turn_num = 0
        prob = 1
        vanilla_reward = -1
        while not self.episode_over and turn_num<self.max_turn:
            # new_turn: (agent_state, agent_action, agent_next_state, user_state, dialogue_end_bool)
            turn_shot = self.next_turn(print_act=print_act, current_hidden_state=current_hidden_state, sampling_bool=sampling_bool)
            # record current state, action and next_state
            current_hidden_state = turn_shot['hidden_state']
            dialogue_traj.append(turn_shot)
            self.episode_over = turn_shot['episode_over']
            turn_num = turn_shot['state_tracker_shot']['turn_count']
            prob *= turn_shot['action']['act_prob']
            vanilla_reward = turn_shot['vanilla_reward']

        return dialogue_traj, prob, vanilla_reward

    def mc_search(self, state_tracker_to_recover=None, user_state_to_recover=None, current_hidden_state=None, print_act=False):
        """
        :param state_tracker: this item includes the current state of the dialogue
        :param dialogue_prefix: this is the prefix of a dialogue, used as the input of MC search.
        :param current_hidden_state: this is the hidden state of RNN at current time step
        :return: the sampled part of the given dialogue, including rule-based state, hidden_state, action and next state
        :return the whole dialogue, which also equals to "dialogue_prefix + sampled_dialogue_traj"
        """
        self.episode_over = False
        # reset the tracker and user status with given screenshots
        self.state_tracker.initialize_spisode_with_tracker_screenshot(tracker_screenshot=state_tracker_to_recover)
        self.running_user.initialize_user_with_saved_state(user_screepshot=user_state_to_recover)
        # start a new dialogue
        sampled_dialogue_traj = []
        turn_num = self.state_tracker.turn_count
        self.agent.current_slot_id = turn_num/2
        # print("start sampling **************")
        # print("mc current sampling turn num", turn_num)
        while not self.episode_over and turn_num<self.max_turn:
            # new_turn: (agent_state, agent_action, agent_next_state, user_state, dialogue_end_bool)
            turn_shot = self.next_turn(print_act=print_act, current_hidden_state=current_hidden_state, sampling_bool=True)
            # record current state, action and next_state
            current_hidden_state = turn_shot['hidden_state']
            self.episode_over = turn_shot['episode_over']
            turn_num = turn_shot['state_tracker_shot']['turn_count']
            sampled_dialogue_traj.append(turn_shot)
        # only the back part is returned
        # print("sampled traj length", len(sampled_dialogue_traj))
        return sampled_dialogue_traj

    def print_function(self, agent_action=None, user_action=None, state_tracker=None):
        """ Print Function """

        if agent_action:
            if dialog_config.run_mode == 0:
                if self.agent.__class__.__name__ != 'AgentCmd':
                    print ("Turn %d sys: %s" % (agent_action['turn'], agent_action['nl']))
            elif dialog_config.run_mode == 1:
                if self.agent.__class__.__name__ != 'AgentCmd':
                    print("Turn %d sys: %s, inform_slots: %s, request slots: %s" % (
                        agent_action['turn'], agent_action['diaact'], agent_action['inform_slots'],
                        agent_action['request_slots']))
            elif dialog_config.run_mode == 2:  # debug mode
                print("Turn %d sys: %s, inform_slots: %s, request slots: %s" % (
                    agent_action['turn'], agent_action['diaact'], agent_action['inform_slots'],
                    agent_action['request_slots']))
                print ("Turn %d sys: %s" % (agent_action['turn'], agent_action['nl']))

            if dialog_config.auto_suggest == 1:
                print(
                        '(Suggested Values: %s)' % (
                    state_tracker.get_suggest_slots_values(agent_action['request_slots'])))
        elif user_action:
            if dialog_config.run_mode == 0:
                print ("Turn %d usr: %s" % (user_action['turn'], user_action['nl']))
            elif dialog_config.run_mode == 1:
                print ("Turn %s usr: %s, inform_slots: %s, request_slots: %s" % (
                    user_action['turn'], user_action['diaact'], user_action['inform_slots'],
                    user_action['request_slots']))
            elif dialog_config.run_mode == 2:  # debug mode, show both
                print ("Turn %d usr: %s, inform_slots: %s, request_slots: %s" % (
                    user_action['turn'], user_action['diaact'], user_action['inform_slots'],
                    user_action['request_slots']))
                print ("Turn %d usr: %s" % (user_action['turn'], user_action['nl']))

            if self.agent.__class__.__name__ == 'AgentCmd':  # command line agent
                user_request_slots = user_action['request_slots']
                if 'ticket' in user_request_slots.keys(): del user_request_slots['ticket']
                if len(user_request_slots) > 0:
                    possible_values = state_tracker.get_suggest_slots_values(user_action['request_slots'])
                    for slot in possible_values.keys():
                        if len(possible_values[slot]) > 0:
                            print('(Suggested Values: %s: %s)' % (slot, possible_values[slot]))
                        elif len(possible_values[slot]) == 0:
                            print('(Suggested Values: there is no available %s)' % (slot))
                else:
                    kb_results = state_tracker.get_current_kb_results()
                    print ('(Number of movies in KB satisfying current constraints: %s)' % len(kb_results))
