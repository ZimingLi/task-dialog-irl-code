import torch
import torch.nn as nn
import torch.nn.functional as F
import copy

class State_Encoder(nn.Module):
    # this is the rnn state tracker which uses (current user input, last system output, turn_num,
    # available_kb_result_num) to update the state tracker.
    # state_input_num = len(current user input, last system output, turn_num, available_kb_result_num)
    def __init__(self, state_input_dim, action_embedding_dim, hidden_dim, use_cuda, n_layers=2, dropout=0):
        super(State_Encoder, self).__init__()
        self.hidden_dim = hidden_dim
        self.use_cuda = use_cuda
        self.action_embed = nn.Linear(state_input_dim, action_embedding_dim)
        # Initialize GRU; the input_size and hidden_size params are both set to 'hidden_size'
        #   because our input size is a word embedding with number of features == hidden_size
        self.gru = nn.GRU(action_embedding_dim , hidden_dim, n_layers,
                          dropout=(0 if n_layers == 1 else dropout), bidirectional=False)
        self.sigmoid = nn.Sigmoid()
        # self.fc = nn.Linear(hidden_dim, vocab_size)
        # self.log_softmax = nn.LogSoftmax(dim=1)
        # self.init_params()


    def forward(self, input_rnn_state_tracker=None, hidden=None):
        # Convert last action index to embeddings
        embedded = self.action_embed(input_rnn_state_tracker)
        state_input = embedded.unsqueeze(0)
        # state_input = embedded
        # Pack padded batch of sequences for RNN module
        # packed = torch.nn.utils.rnn.pack_padded_sequence(embedded, input_lengths)
        # Forward pass through GRU
        # print("state_enc",state_input.size())
        outputs, hidden = self.gru(state_input, hidden)
        # print("state_enc",hidden.size())
        # Return output and final hidden state
        return outputs, hidden

################################################################
class Utterance_Encoder(nn.Module):
    def __init__(self, vocab_size, embedding_dim, hidden_dim, use_cuda, n_layers=2, dropout=0):
        super(Utterance_Encoder, self).__init__()
        self.hidden_dim = hidden_dim
        self.use_cuda = use_cuda
        self.embedding = nn.Embedding(vocab_size, embedding_dim)
        # Initialize GRU; the input_size and hidden_size params are both set to 'hidden_size'
        #   because our input size is a word embedding with number of features == hidden_size
        self.gru = nn.GRU(embedding_dim, hidden_dim, n_layers,
                          dropout=(0 if n_layers == 1 else dropout), bidirectional=True)
        self.fc = nn.Linear(hidden_dim, vocab_size)
        self.log_softmax = nn.LogSoftmax(dim=1)
        self.init_params()


    def forward(self, input_seq, input_lengths, hidden=None):
        # Convert word indexes to embeddings
        embedded = self.embedding(input_seq)
        # Pack padded batch of sequences for RNN module
        packed = torch.nn.utils.rnn.pack_padded_sequence(embedded, input_lengths)
        # Forward pass through GRU
        outputs, hidden = self.gru(packed, hidden)
        # Unpack padding
        outputs, _ = torch.nn.utils.rnn.pad_packed_sequence(outputs)
        # Sum bidirectional GRU outputs
        outputs = outputs[:, :, :self.hidden_size] + outputs[:, : ,self.hidden_size:]
        # Return output and final hidden state
        return outputs, hidden


