'''
Created on Oct 30, 2017

An DQN Agent modified for DDQ Agent

Some methods are not consistent with super class Agent.

@author: Baolin Peng
'''

import random, copy, json
import pickle as pickle
import numpy as np
from collections import namedtuple, deque

from deep_dialog import dialog_config
import torch.nn as nn
from .agent import Agent
from deep_dialog.adv_learning import PolicyNet
import deep_dialog.dialog_config as dialog_config

import torch
import torch.optim as optim
import torch.nn.functional as F

# DEVICE = torch.device('cpu')

Transition = namedtuple('Transition', ('state', 'action', 'reward', 'next_state', 'term'))


class AgentMLP(Agent):
    def __init__(self, movie_kb=None, act_set=None, slot_set=None, params=None, policy_input_dim=100, rnn_tracker=None,
                 device='cpu'):
        self.movie_kb = movie_kb
        self.act_set = act_set
        self.slot_set = slot_set
        self.act_cardinality = len(act_set.keys())
        self.slot_cardinality = len(slot_set.keys())

        self.feasible_actions = dialog_config.feasible_actions
        self.num_actions = len(self.feasible_actions)

        self.epsilon = params['epsilon']
        self.agent_run_mode = params['agent_run_mode']
        self.agent_act_level = params['agent_act_level']

        self.kb_result_dimension = 10

        self.hidden_size = params.get('dqn_hidden_size', 60)
        self.warm_start = params.get('warm_start', 0)

        self.max_turn = params['max_turn']
        self.device = device
        self.rule_data = False
        # self.state_dimension = 2 * self.act_cardinality + 7 * self.slot_cardinality + 3 + self.max_turn
        self.input_dim = 2 * self.act_cardinality + 4 * self.slot_cardinality + self.max_turn + dialog_config.kb_result_dim
        self.policy_net = PolicyNet.Policy_Net(policy_input_dim, self.num_actions, rnn_tracker, dropout=0.3).to(device)
        # self.policy_net = self.policy_net.to(device)
        self.loss_function = PolicyNet.PG_Loss().to(device)
        self.criterion = nn.CrossEntropyLoss(reduction='none')

        # self.target_dqn = DQN(self.state_dimension, self.hidden_size, self.num_actions).to(DEVICE)
        # self.target_dqn.load_state_dict(self.dqn.state_dict())
        # self.target_dqn.eval()

        # self.optimizer = optim.RMSprop(self.dqn.parameters(), lr=1e-3)

        self.cur_bellman_err = 0

        # Prediction Mode: load trained DQN model
        '''
        if params['trained_model_path'] != None:
            self.load(params['trained_model_path'])
            self.predict_mode = True
            self.warm_start = 2
        '''

    def initialize_episode(self):
        """ Initialize a new episode. This function is called every time a new episode is run. """

        self.current_slot_id = 0
        self.phase = 0
        self.request_set = ['moviename', 'starttime', 'city', 'date', 'theater', 'numberofpeople']

    def state_to_action(self, state_input, current_hidden_state, sampling_bool):
        """ DQN: Input state, output action """
        # self.state['turn'] += 2
        self.action, prob, hidden_state = self.run_policy(state_input, current_hidden_state, sampling_bool)
        # action index to real action
        act_slot_response = copy.deepcopy(self.feasible_actions[self.action])
        return {'act_slot_response': act_slot_response, 'act_slot_value_response': None, 'act_prob': prob}, self.action, hidden_state

    def run_policy(self, representation, current_hidden_state, sampling_bool):
        if random.random() < self.epsilon:
            return random.randint(0, self.num_actions - 1), 'hidden_holder'
        else:
            if self.warm_start == 1 or self.rule_data:
                return self.rule_policy()
            else:
                return self.adv_policy(representation, current_hidden_state, sampling_bool)

    def rule_policy(self):
        """ Rule Policy """
        act_slot_response = {}

        if self.current_slot_id < len(self.request_set):
            slot = self.request_set[self.current_slot_id]
            self.current_slot_id += 1
            act_slot_response = {}
            act_slot_response['diaact'] = "request"
            act_slot_response['inform_slots'] = {}
            act_slot_response['request_slots'] = {slot: "UNK"}
        elif self.phase == 0:
            act_slot_response = {'diaact': "inform", 'inform_slots': {'taskcomplete': "PLACEHOLDER"},
                                 'request_slots': {}}
            self.phase += 1
        elif self.phase == 1:
            act_slot_response = {'diaact': "thanks", 'inform_slots': {}, 'request_slots': {}}

        return self.action_index(act_slot_response), 0, 'hidden_holder'

    def adv_policy(self, state_representation, current_hidden_state, sampling_bool):
        """ Return action from PolicyNet"""
        with torch.no_grad():
            # TODO: state_representation->torch.tensor
            if sampling_bool:
                action, prob, hidden = self.policy_net.next_action_sample(state_representation.to(self.device),
                                                                    current_hidden_state)
            else:
                action, prob, hidden = self.policy_net.next_action_greedy(state_representation.to(self.device),
                                                                    current_hidden_state)

        return action, prob, copy.deepcopy(hidden)

    def action_index(self, act_slot_response):
        """ Return the index of action """

        for (i, action) in enumerate(self.feasible_actions):
            if act_slot_response == action:
                return i
        print(act_slot_response)
        raise Exception("action index not found")

    def train(self, state_tracker_input=None, target_action=None, reward=None, mask=None):
        hidden_state = None
        crossent_loss_all = 0
        reg_loss_all = 0
        mask = mask.permute(1, 0)
        for step_num in range(self.max_turn / 2):
            input = state_tracker_input[step_num]
            target = target_action[step_num]
            mask_pos = mask[step_num]
            reward_pos = reward[step_num]  # max_turn * batch_size
            reward_pos = reward_pos[step_num:, :].sum(dim=0)  # sum the future steps after time t
            reward_pos = reward_pos.view(-1)
            # print(len(state_tracker_input),input.size(),target_action.size(), target.size())
            output, distribution, hidden_state = self.policy_net.forward(input, hidden_state)
            # print(distribution)
            crossent_loss, reg_loss = self.causal_loss(input=output, target=target, reward=reward_pos, mask=mask_pos)
            # loss_each, num_each = self.maskNLLLoss(input=output, target=target, reward=reward_pos, mask=mask_pos)
            # loss_cur = self.loss_function.sequence_loss_by_example(log_distribution, target, mask, reward=reward[])
            # loss += criterion(output, target)
            crossent_loss_all += crossent_loss
            reg_loss_all += reg_loss
        return crossent_loss_all, reg_loss_all

    def gan_train(self, state_tracker_input=None, target_action=None, reward=None, mask=None):
        '''
        :param state_tracker_input: len * batch_size * dim
        :param target_action: len * batch_size
        :param reward:
        :param mask:
        :return:
        '''
        hidden_state = None
        crossent_loss_all = 0
        reg_loss_all = 0
        mask = mask.permute(1, 0)
        for step_num in range(self.max_turn / 2):
            input = state_tracker_input[step_num]
            target = target_action[step_num]
            mask_pos = mask[step_num]
            reward_pos = reward[step_num]  # max_turn * batch_size
            reward_pos = reward_pos.view(-1)
            # print(len(state_tracker_input),input.size(),target_action.size(), target.size())
            output, distribution, hidden_state = self.policy_net.forward(input, hidden_state)
            # print(distribution)
            crossent_loss, reg_loss = self.causal_loss(input=output, target=target, reward=reward_pos, mask=mask_pos)
            crossent_loss_all += crossent_loss
            reg_loss_all += reg_loss
        return crossent_loss_all, reg_loss_all

    def maskNLLLoss(self, input, target, reward, mask):
        criterion = nn.CrossEntropyLoss()
        nTotal = mask.sum()
        # crossEntropy = -torch.log(torch.gather(input, 1, target.view(-1, 1)))
        crossEntropy = criterion(input, target)
        reward_loss = torch.mul(crossEntropy, reward)
        loss = reward_loss.masked_select(mask).mean()
        return loss, nTotal.item()

    def causal_loss(self, input, target, reward, mask):
        criterion = nn.CrossEntropyLoss(reduction='none')
        crossEntropy = criterion(input, target)
        # (log(p))
        loss = -crossEntropy
        # log(p) +1
        ent_loss_1 = loss + 1
        ent_loss_2 = ent_loss_1.detach()
        # if seq_len is fixed, mask==1
        # log(p) * (log(p) + 1) = -log(p) * (-log(p) - 1)
        ent_loss = ent_loss_2 * loss
        # print(ent_loss)
        policy_ent_loss = torch.mul(ent_loss, mask).mean()
        # print(policy_ent_loss)
        # policy_ent_loss = ent_loss.masked_select(mask).mean()
        # -R * log(p)
        reward_loss = torch.mul(crossEntropy, reward)
        # print(reward_loss)
        # print(mask)
        # crossent_loss = reward_loss.masked_select(mask).mean()
        crossent_loss = torch.mul(reward_loss, mask).mean()
        # print(crossent_loss)
        return crossent_loss, policy_ent_loss

    ################################################################################
    #    Debug Functions
    ################################################################################

    def load_experience_replay_from_file(self, path):
        """ Load the experience replay pool from a file"""

        self.experience_replay_pool = pickle.load(open(path, 'rb'))

    def load_trained_DQN(self, path):
        """ Load the trained DQN from a file """

        trained_file = pickle.load(open(path, 'rb'))
        model = trained_file['model']
        print ("Trained DQN Parameters:", json.dumps(trained_file['params'], indent=2))
        return model

    def set_user_planning(self, user_planning):
        self.user_planning = user_planning

    def save(self, filename):
        torch.save(self.dqn.state_dict(), filename)

    def load(self, filename):
        self.dqn.load_state_dict(torch.load(filename))

    def reset_dqn_target(self):
        self.target_dqn.load_state_dict(self.dqn.state_dict())
