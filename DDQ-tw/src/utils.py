import numpy

def extract_score(file_name,target_name):
    with open(file_name,'r') as fp:
        with open(target_name, 'w') as ft:
            line = fp.readline()
            while line:
                line_list = line.strip().split()
                if line_list[0]=='==========':
                    ft.write(line)
                line = fp.readline()

def main_extract_score():
    file_name = './logs/irl_232198_1.log'
    target_name = './result/irl_fold_1.log'
    extract_score(file_name, target_name)

    file_name = './logs/irl_232198_2.log'
    target_name = './result/irl_fold_2.log'
    extract_score(file_name, target_name)

    file_name = './logs/irl_232198_3.log'
    target_name = './result/irl_fold_3.log'
    extract_score(file_name, target_name)

    file_name = './logs/irl_232198_4.log'
    target_name = './result/irl_fold_4.log'
    extract_score(file_name, target_name)

    file_name = './logs/irl_232198_5.log'
    target_name = './result/irl_fold_5.log'
    extract_score(file_name, target_name)


def extract_tc_score(file_name, target_name):
    episode= 0
    with open(file_name,'r') as fp:
        with open(target_name, 'w') as ft:
            line = fp.readline()
            while line:
                line_list = line.strip().split()
                if line_list[0] == '======':
                    line = fp.readline()
                    ft.write('Episode: {} '.format(episode) + line)
                    episode += 10
                line = fp.readline()

def main_extract_tc_score():
    file_name = './logs/tc_data_240338_6.log'
    target_name = './result/tc_fold_1.log'
    extract_tc_score(file_name, target_name)

    file_name = './logs/tc_data_235486_7.log'
    target_name = './result/tc_fold_2.log'
    extract_tc_score(file_name, target_name)

    file_name = './logs/tc_data_234380_8.log'
    target_name = './result/tc_fold_3.log'
    extract_tc_score(file_name, target_name)

    file_name = './logs/tc_data_237889_9.log'
    target_name = './result/tc_fold_4.log'
    extract_tc_score(file_name, target_name)

    file_name = './logs/tc_data_239199_10.log'
    target_name = './result/tc_fold_5.log'
    extract_tc_score(file_name, target_name)

if __name__ == "__main__":
    main_extract_tc_score()